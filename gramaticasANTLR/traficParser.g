grammar traficParser;

@header {
package simulationControl.parsers;
import java.util.HashMap;
import java.util.Vector;
import network.Pair;
import network.Node;
import network.RequestGenerator;

}

@lexer::header {
package simulationControl.parsers;
}

@members {
private Vector<Pair> pairs;
private HashMap<String,Node> nodes;
private Pair actualPair;

public Vector<Pair> getPairs() {
	return pairs;
}
public void setNodes(Vector<Node> nodes){
	this.nodes = new HashMap<String,Node>();
	for(Node n : nodes){
		this.nodes.put(n.getName(),n);
	}
}

}


initial	
@init{
	pairs = new Vector<Pair>();
}	
	:	
		(descricaoGeradoresPar)*
	;


descricaoGeradoresPar
	:	
		par ('\n') ( descricaoGerador ('\n'))*
	
	;

par	:
		src = ID '->' dest = ID
		{
			Node o = nodes.get($src.text);
			Node d = nodes.get($dest.text);
			actualPair = new Pair(o,d);	
			pairs.add(actualPair);	
		}
	;

descricaoGerador
	:	
		'\t' lb = larguraDeBanda ';' arrive = FLOAT ';' hold = FLOAT ';' inc = FLOAT	
		{
			RequestGenerator rg = new RequestGenerator(actualPair, $lb.bandwidth, Double.parseDouble($hold.text),Double.parseDouble($arrive.text), Double.parseDouble($inc.text));
			actualPair.addRequestGenerator(rg);
		}
	;

larguraDeBanda returns [Double bandwidth]
	:	
		f = FLOAT (('Gbps' {$bandwidth = Double.parseDouble($f.text) * 1073741824.0;})|('Mbps' {$bandwidth = Double.parseDouble($f.text) * 1048576.0;}))
	;

ID  :	('a'..'z'|'A'..'Z'|'0'..'9'|'_')+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

