grammar networkParser;


@header {
package simulationControl.root.parsers;
import java.util.HashMap;
import network.Node;
import network.Link;
import java.util.Vector;
}

@lexer::header {
package simulationControl.root.parsers;
}

@members {
private HashMap<String,Node> nodes;
public Vector<Node> getNodes() {
	return new Vector<Node>(nodes.values());
}
}





initial	
@init{
	nodes = new HashMap<>();
}
	:	

	nodesDescription
	linksDescription

	;

nodesDescription
	:	
		'node:' '\n'(INT{
				Node n = new Node($INT.text);
				nodes.put(n.getName(),n);		
				} '\n')*
	
	
	;
	
	
linksDescription
	:
	'links:' '\n'
	(linkDescription '\n')*
	
	;
	
linkDescription
	:
		source = INT';'dest = INT';'cost = FLOAT';'slots = INT';'slotLB = FLOAT';'dist = INT
		{
			Node src = nodes.get($source.text);
			Node d = nodes.get($dest.text);
			Double c = Double.parseDouble($cost.text);
			int s = Integer.parseInt($slots.text);
			Double slb = Double.parseDouble($slotLB.text);
			int dis = Integer.parseInt($dist.text);
			
			src.getOxc().addLink(new Link(src.getOxc(), d.getOxc(), c, s, slb,dis));
		
		}
		
	;	


INT :	'0'..'9'+
    ;
    
FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;
    
fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;
    

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

