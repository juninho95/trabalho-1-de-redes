grammar zonesParser;

@header {
package simulationControl.parsers;
import java.util.List;
import java.util.ArrayList;
}

@lexer::header {
package simulationControl.parsers;
}


@members {
private List<int[]> zones = new ArrayList<int[]>();

public List<int[]> getZones(){
	return zones;
}
}

initial	:	
		zone+
	;
	
zone	:	
	slot = INT ':' '<' ini = INT '-' fim = INT '>'
	{
		int aux[] = new int[3];
		aux[0] = Integer.parseInt($slot.text);
		aux[1] = Integer.parseInt($ini.text);
		aux[2] = Integer.parseInt($fim.text);
		zones.add(aux);
	} 
	;


INT :	'0'..'9'+
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    |   '\n' {$channel=HIDDEN;}
    |   '\r' {$channel=HIDDEN;}
    ;

