grammar simulationParser;



tokens{
	INTEGRATED = 'integrated';
	SEQUENCIAL = 'sequencial';
	REQUESTSTAG = 'requests';
	REPLICATIONSTAG = 'replications';
	RSATAG = 'rsa';
	LOADINCTAG = 'loadPoints';
	SIGNIFICANCETAG = 'significance';
	}
	


@header {
package simulationControl.root.parsers;
import simulator.Simulation;
import rsa.RSA;
}

@lexer::header {
package simulationControl.root.parsers;
}

@members{
	
	private int requests;
	private int rsaType;
	private String routingAlgo;
	private String spectrumAssignAlgo;
	private String rsaIntegratedAlgo;
	private int loadPoints;
	private int replications;
	private double significance;
	
	private boolean breq = false, brsa = false, bload = false, brep = false, bsig = false;
	
	
	public int getRequests() {
		return requests;
	}
	public int getRsaType() {
		return rsaType;
	}
	public String getRoutingAlgo() {
		return routingAlgo;
	}
	public String getSpectrumAssignAlgo() {
		return spectrumAssignAlgo;
	}
	public String getRsaIntegratedAlgo() {
		return rsaIntegratedAlgo;
	}
	public int getLoadPoints() {
		return loadPoints;
	}
	public int getReplications() {
		return replications;
	}
	public double getSignificance() {
		return significance;
	}
	
	
}


initial:	
	
	((request | rsaDefinition | load | replications | significance) '\n') *
	
	{
		if(breq && brsa && bload && brep && bsig){
		}else{
			throw new RecognitionException();
		}	
		/*System.out.println("rsaType: " + rsaType);
		System.out.println("rout: " + routingAlgo);		
		System.out.println("spec: " + spectrumAssignAlgo);*/
		
	}
	
	;
	
request	:
	{
		if(breq) throw new RecognitionException();
		else breq = true;
	}	
	REQUESTSTAG ':' quant = INT{ 
				     requests = Integer.parseInt($quant.text);
				   }
	;	

rsaDefinition
	:
	{
		
		if(brsa){ throw new RecognitionException();}
		else{ 
			brsa = true;
			
		}
	}	
	RSATAG ':' ((SEQUENCIAL ';' routing = ID ';' spectrumAssign = ID){
										
										rsaType = RSA.RSA_SEQUENCIAL;
										
										try{
											
											routingAlgo = $routing.text;
											
											spectrumAssignAlgo = $spectrumAssign.text;
										}catch(Exception ex){
											throw new RecognitionException();
										
										}
									 }
	
		   |(INTEGRATED ';' integrated = ID){
		   						
		   						rsaType = RSA.RSA_INTEGRATED;
		   						try{
									
									rsaIntegratedAlgo = $integrated.text;
								}catch(Exception ex){
									throw new RecognitionException();										
								}		   
		   					
		   					})	
	
	;
	
load	:	
	{
		if(bload) throw new RecognitionException();
		else bload = true;
	}	
	LOADINCTAG ':' linc = INT{
					loadPoints = Integer.parseInt($linc.text);					
				 }
	;

replications
	:	
	{
		if(brep) throw new RecognitionException();
		else brep = true;
	}	
	REPLICATIONSTAG ':' r = INT{
					replications = Integer.parseInt($r.text);
				   }
	;

significance
	:	
	{
		if(bsig) throw new RecognitionException();
		else bsig = true;
	}	
	SIGNIFICANCETAG ':' d = FLOAT{
				     	significance = Double.parseDouble($d.text);
				     }	
	;

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :	'0'..'9'+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;


WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;
