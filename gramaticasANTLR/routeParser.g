grammar routeParser;

initial	:
	(routeDescription '\n')*

	;

routeDescription
	:
	
	ID '\t' (ID '\t')* ID
	
	;




ID  :	('a'..'z'|'A'..'Z'|'0'..'9'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

