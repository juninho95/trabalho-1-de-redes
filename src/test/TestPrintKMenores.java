package test;

import java.util.List;
import java.util.Vector;

import network.Mesh;
import network.Node;

import rsa.KMenores;
import rsa.NewKMenores;
import rsa.Route;
import simulationControl.parsers.NetworkFileReader;

public class TestPrintKMenores {
	
	public static void main(String args[]) throws Exception{
		Vector<Node> nodes =  NetworkFileReader.readNetwork(args[0] + "/network");
		
		Mesh m = new Mesh(nodes, null, "djk", "firstfit");
		
		NewKMenores km = new NewKMenores(m, 5);
		
		for (Node n1 : nodes) {
			for (Node n2 : nodes) {
				if(n1==n2) continue;
				System.out.println("rotas entre: " + n1.getName() + " e " + n2.getName() );
				printRoutes(km.getRoutes(n1, n2));
			}
		}
		
	}

	private static void printRoutes(List<Route> routes) {
		for (Route route : routes) {
			String aux = "";
			for (Node n : route.getNodeList()) {
				aux = aux + n.getName() + "-";
			}
			System.out.println(aux);
		}
		
	}
	
	
}
