package test;

import java.util.Vector;

import measurement.Measurements;
import network.ControlPlane;
import network.Link;
import network.Mesh;
import network.Node;
import network.Pair;

import org.junit.Assert;
import org.junit.Test;

import request.Request;

import rsa.Route;
import rsa.spectrumAssignment.BestFit;
import rsa.spectrumAssignment.FirstFit;
import simulationControl.parsers.NetworkFileReader;
import simulationControl.parsers.TraficFileReader;

public class BestFitTest {

	@Test
	public void testAssignSpectrum() {
		
		//preparando cen�rio 1
		Link l1 = new Link(null, null, 1.0, 400,12500000000.0, 100.0);
		Link l2 = new Link(null, null, 1.0, 400,12500000000.0, 100.0);
		usarFaixas1(l1);
		usarFaixas2(l2);
		Route r = new Route(new Vector<Node>());
		Vector<Link> vl = new Vector<>();
		vl.add(l1);
		vl.add(l2);
		r.setLinkList(vl);
		Request request = new Request();
		request.setRoute(r);
		
		BestFit b = new BestFit();
		
		
		
		Assert.assertTrue(b.assignSpectrum(33, request)); //o m�todo deve alocar o espectro e retornar true
		
		//nova faixa livre em l1
		Assert.assertEquals(151, l1.getFreeSpectrumBands().get(1)[0]);
		Assert.assertEquals(215, l1.getFreeSpectrumBands().get(1)[1]);
		Assert.assertEquals(249, l1.getFreeSpectrumBands().get(2)[0]);
		Assert.assertEquals(250, l1.getFreeSpectrumBands().get(2)[1]);
	
		//novas faixas livres em l2
		Assert.assertEquals(249, l2.getFreeSpectrumBands().get(3)[0]);
		Assert.assertEquals(350, l2.getFreeSpectrumBands().get(3)[1]);
		
	}
	
	
	
	
	
	
	private void usarFaixas1(Link l){
		
		int i1[] = {101,150};
		int i2[] = {251,310};
		
		l.useSpectrum(i1);
		l.useSpectrum(i2);
		
	}
	
	private void usarFaixas2(Link l){
		
		int i1[] = {31,70};
		int i2[] = {101,130};
		int i3[] = {201, 215};
		int i4[] = {351,400};
		
		l.useSpectrum(i1);
		l.useSpectrum(i2);
		l.useSpectrum(i3);
		l.useSpectrum(i4);
		
	}

}
