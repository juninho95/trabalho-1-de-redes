package test;

import static org.junit.Assert.*;
import junit.framework.Assert;

import measurement.ProbabilidadeDeBloqueio;
import network.Node;
import network.Pair;

import org.junit.Test;

import request.Request;

public class ProbabilidadeDeBloqueioTest {

	@Test
	public void testAddNewObservation() {
		ProbabilidadeDeBloqueio pb = new ProbabilidadeDeBloqueio(1, 1);
		
	}

	@Test
	public void testGetProbBlockGeral() {
		ProbabilidadeDeBloqueio pb = new ProbabilidadeDeBloqueio(1, 1);
		
		Pair p1 = new Pair(new Node("1"), new Node("2"));
		Pair p2 = new Pair(new Node("3"), new Node("4"));
		
		Request r1 = new Request();
		r1.setPair(p1);
		Request r2 = new Request();
		r2.setPair(p1);
		Request r3 = new Request();
		r3.setPair(p2);
		Request r4 = new Request();
		r4.setPair(p2);
		
		pb.addNewObservation(true, r1);
		pb.addNewObservation(true, r3);
		pb.addNewObservation(false, r2);
		pb.addNewObservation(false, r4);
		
		Assert.assertEquals(0.5, pb.getProbBlockGeral());	
		
	}

	@Test
	public void testGetProbBlockPair() {
ProbabilidadeDeBloqueio pb = new ProbabilidadeDeBloqueio(1, 1);
		
		Pair p1 = new Pair(new Node("1"), new Node("2"));
		Pair p2 = new Pair(new Node("3"), new Node("4"));
		
		Request r1 = new Request();
		r1.setPair(p1);
		Request r2 = new Request();
		r2.setPair(p1);
		Request r3 = new Request();
		r3.setPair(p2);
		Request r4 = new Request();
		r4.setPair(p2);
		
		pb.addNewObservation(true, r1);
		pb.addNewObservation(true, r3);
		pb.addNewObservation(false, r2);
		pb.addNewObservation(false, r4);
		
		Assert.assertEquals(0.5, pb.getProbBlockPair(p1));
		Assert.assertEquals(0.5, pb.getProbBlockPair(p2));
	}

	@Test
	public void testGetProbBlockBandwidth() {
ProbabilidadeDeBloqueio pb = new ProbabilidadeDeBloqueio(1, 1);
		
		Pair p1 = new Pair(new Node("1"), new Node("2"));
		Pair p2 = new Pair(new Node("3"), new Node("4"));
		
		Request r1 = new Request();
		r1.setPair(p1);
		r1.setRequiredBandwidth(40000000000.0);
		Request r2 = new Request();
		r2.setPair(p1);
		r2.setRequiredBandwidth(40000000000.0);
		Request r3 = new Request();
		r3.setPair(p2);
		r3.setRequiredBandwidth(80000000000.0);
		Request r4 = new Request();
		r4.setPair(p2);
		r4.setRequiredBandwidth(80000000000.0);
		
		pb.addNewObservation(true, r1);
		pb.addNewObservation(true, r3);
		pb.addNewObservation(false, r2);
		pb.addNewObservation(false, r4);
		
		Assert.assertEquals(0.5, pb.getProbBlockBandwidth(40000000000.0));
		Assert.assertEquals(0.5, pb.getProbBlockBandwidth(80000000000.0));
	}

	@Test
	public void testGetProbBlockPairBandwidth() {
		fail("Not yet implemented");
	}

}
