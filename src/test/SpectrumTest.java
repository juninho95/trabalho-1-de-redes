package test;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;

import network.Spectrum;

import org.junit.Assert;
import org.junit.Test;

public class SpectrumTest {



	@Test
	public void testUseSpectrum() {
		
		Spectrum spectrum = new Spectrum(400,12.5);
		int use[] = {101,200};
		
		spectrum.useSpectrum(use);
		
		List<int[]> freeSpecs = spectrum.getFreeSpectrumBands();
		
		int expected1[] = {1,100};
		
		int expected2[] = {201,400};
		
		assertArrayEquals(freeSpecs.get(0), expected1);
		assertArrayEquals(freeSpecs.get(1), expected2);
		
		
	}

	@Test
	public void testFreeSpectrum() {
		
		Spectrum spectrum;
		int use1[] = {1, 100};
		int use2[] = {101,200};
		int use3[] = {201,300};
		
		//sem merge
		spectrum = new Spectrum(400,12.5);
		spectrum.useSpectrum(use1);
		spectrum.useSpectrum(use2);
		spectrum.useSpectrum(use3);
		spectrum.freeSpectrum(use2);		
		int expected1[] = use2;
		int expected2[] = {301,400};
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(0), expected1);
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(1), expected2);
		
		
		//com merge para tr�s
		spectrum = new Spectrum(400,12.5);
		spectrum.useSpectrum(use1);
		spectrum.useSpectrum(use2);
		spectrum.useSpectrum(use3);
		spectrum.freeSpectrum(use1);
		spectrum.freeSpectrum(use2);
		int expected3[] = {1,200};
		int expected4[] = {301,400};
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(0), expected3);
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(1), expected4);
		
		//com merge para frente
		spectrum = new Spectrum(400,12.5);
		spectrum.useSpectrum(use1);
		spectrum.useSpectrum(use2);
		spectrum.useSpectrum(use3);
		spectrum.freeSpectrum(use2);
		spectrum.freeSpectrum(use1);
		int expected5[] = {1,200};
		int expected6[] = {301,400};
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(0), expected5);
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(1), expected6);
		
		//com merge para os dois lados
		spectrum = new Spectrum(400,12.5);
		spectrum.useSpectrum(use1);
		spectrum.useSpectrum(use2);
		spectrum.useSpectrum(use3);
		spectrum.freeSpectrum(use2);
		spectrum.freeSpectrum(use3);
		int expected7[] = {101,400};
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(0), expected7);
		
		
	}

	@Test
	public void testGetFreeSpectrumBands() {
		
		Spectrum spectrum = new Spectrum(400,12.5);
		
		int expected[] = {1,400};
		
		assertArrayEquals(spectrum.getFreeSpectrumBands().get(0), expected);
		
	}
	
	@Test
	public void testUtilization(){
		
		Spectrum spectrum = new Spectrum(400,12.5);
		
		Assert.assertEquals(0.0, spectrum.utilization(), 0.0);
		
		int use1[] = {101,200};
		spectrum.useSpectrum(use1);
		
		Assert.assertEquals(0.25, spectrum.utilization(), 0.0);
		
		
	}

}
