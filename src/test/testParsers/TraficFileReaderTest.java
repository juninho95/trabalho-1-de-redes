package test.testParsers;

import static org.junit.Assert.*;

import java.util.Vector;

import network.Node;
import network.Pair;

import org.junit.Assert;
import org.junit.Test;

import simulationControl.parsers.NetworkFileReader;
import simulationControl.parsers.TraficFileReader;

public class TraficFileReaderTest {

	//o NetworkFileReader deve estar funcionando corretamente antes de fazer este teste!!!
	
	@Test
	public void testReadTrafic() throws Exception{
		
			Vector<Node> nodes = NetworkFileReader.readNetwork("./src/test/testParsers/inputNetworkFile");
			Vector<Pair> pairs = TraficFileReader.readTrafic("./src/test/testParsers/inputTraficFile", nodes);
			
			Assert.assertEquals(12, pairs.size());
			
		
	}

}
