package test.testParsers;

import static org.junit.Assert.*;

import java.util.Vector;

import measurement.Measurements;
import network.Mesh;
import network.Node;

import org.junit.Assert;
import org.junit.Test;

import simulationControl.parsers.NetworkFileReader;

public class NetworkFileReaderTest {

	@Test
	public void testReadNetwork(){
		Vector<Node> nodes;
		try {
			nodes = NetworkFileReader.readNetwork("./src/test/testParsers/inputNetworkFile");
			Mesh mesh = new Mesh(nodes, null, "djk", "firstfit");
			Measurements measurements = new Measurements(100, 1, 1);
			mesh.setMeasurements(measurements);
			
			Assert.assertEquals(4, mesh.getNodeList().size());
			Assert.assertEquals(8, mesh.getLinkList().size());
			Assert.assertEquals(1.0, mesh.getLinkList().get(0).getCost(), 0.0);
			Assert.assertEquals(100.0, mesh.getLinkList().get(0).getDistance(), 0.0);
		} catch (Exception e) {
			fail();
			e.printStackTrace();
		}
		
		
		
	}

}
