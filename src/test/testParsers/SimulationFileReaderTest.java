package test.testParsers;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;
import org.omg.PortableInterceptor.SUCCESSFUL;

import rsa.RSA;
import simulationControl.parsers.SimulationFileReader;

public class SimulationFileReaderTest {

	@Test
	public void testSimulationFileReader() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
		
	}

	@Test
	public void testGetRequests() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(100000, sfr.getRequests());		
		
	}

	@Test
	public void testGetRsaType() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(RSA.RSA_SEQUENCIAL, sfr.getRsaType());		
		
	}

	@Test
	public void testGetRoutingAlgo() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(RSA.ROUTING_DJK, sfr.getRoutingAlgo());		
		
	}

	@Test
	public void testGetSpectrumAssignAlgo() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(RSA.SPECTRUM_ASSIGNMENT_FISTFIT, sfr.getSpectrumAssignAlgo());		
		
	}

	@Test
	public void testGetRsaIntegratedAlgo() {
		//caso n�o testado
	}

	@Test
	public void testGetLoadPoints() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(5, sfr.getLoadPoints());		
		
	}

	@Test
	public void testGetReplications() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(10, sfr.getReplications());		
		
	}

	@Test
	public void testGetSignificance() throws Exception {
		
			SimulationFileReader sfr = new SimulationFileReader("./src/test/testParsers/inputSimulationFile");
			Assert.assertEquals(0.05, sfr.getSignificance());		
		
	}

}
