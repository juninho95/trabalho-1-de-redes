package test;

import java.util.Vector;

import measurement.Measurements;
import network.Link;
import network.Mesh;
import network.Node;
import network.Pair;

import org.junit.Assert;
import org.junit.Test;

import request.Request;
import rsa.Route;
import rsa.routing.DJK;

public class DJKTest {

	@Test
	public void testFindRoute() {
		
		Node n1 = new Node("1");
		Node n2 = new Node("2");
		Node n3 = new Node("3");
		Node n4 = new Node("4");
		Node n5 = new Node("5");
		Node n6 = new Node("6");
		
		n1.getOxc().addLink(new Link(n1.getOxc(), n2.getOxc(), 1.0, 400,12500000000.0, 7.0));
		n1.getOxc().addLink(new Link(n1.getOxc(), n3.getOxc(), 1.0, 400,12500000000.0, 9.0));
		n1.getOxc().addLink(new Link(n1.getOxc(), n6.getOxc(), 1.0, 400,12500000000.0, 14.0));
		
		n2.getOxc().addLink(new Link(n2.getOxc(), n1.getOxc(), 1.0, 400,12500000000.0, 7.0));
		n2.getOxc().addLink(new Link(n2.getOxc(), n3.getOxc(), 1.0, 400,12500000000.0, 10.0));
		n2.getOxc().addLink(new Link(n2.getOxc(), n4.getOxc(), 1.0, 400,12500000000.0, 15.0));
		
		n3.getOxc().addLink(new Link(n3.getOxc(), n1.getOxc(), 1.0, 400,12500000000.0, 9.0));
		n3.getOxc().addLink(new Link(n3.getOxc(), n2.getOxc(), 1.0, 400,12500000000.0, 10.0));
		n3.getOxc().addLink(new Link(n3.getOxc(), n6.getOxc(), 1.0, 400,12500000000.0, 2.0));
		n3.getOxc().addLink(new Link(n3.getOxc(), n4.getOxc(), 1.0, 400,12500000000.0, 11.0));
		
		n4.getOxc().addLink(new Link(n4.getOxc(), n2.getOxc(), 1.0, 400,12500000000.0, 15.0));
		n4.getOxc().addLink(new Link(n4.getOxc(), n3.getOxc(), 1.0, 400,12500000000.0, 11.0));
		n4.getOxc().addLink(new Link(n4.getOxc(), n5.getOxc(), 1.0, 400,12500000000.0, 6.0));
		
		n5.getOxc().addLink(new Link(n5.getOxc(), n4.getOxc(), 1.0, 400,12500000000.0, 6.0));
		n5.getOxc().addLink(new Link(n5.getOxc(), n6.getOxc(), 1.0, 400,12500000000.0, 9.0));
		
		n6.getOxc().addLink(new Link(n6.getOxc(), n5.getOxc(), 1.0, 400,12500000000.0, 9.0));
		n6.getOxc().addLink(new Link(n6.getOxc(), n3.getOxc(), 1.0, 400,12500000000.0, 2.0));
		n6.getOxc().addLink(new Link(n6.getOxc(), n1.getOxc(), 1.0, 400,12500000000.0, 14.0));
		
		Vector<Node> nodeList = new Vector<>();
		nodeList.add(n1);
		nodeList.add(n2);
		nodeList.add(n3);
		nodeList.add(n4);
		nodeList.add(n5);
		nodeList.add(n6);
		
		Mesh mesh = new Mesh(nodeList, null, "djk", "firstfit");
		Measurements measurements = new Measurements(100, 1, 1);
		mesh.setMeasurements(measurements);
		Request request = new Request();		
		request.setPair(new Pair(n1, n6));
		
		//come�ar os testes aqui		
		DJK djk = new DJK();		
		djk.computeAllRoutes(mesh);			
		djk.findRoute(request, mesh);
		Route route = request.getRoute();
		
		Assert.assertEquals(route.getHops(), 2);
		
		
	}
	
}
	