package request;

import network.Node;
import network.Pair;
import network.RequestGenerator;
import rsa.Route;
import rsa.modulation.Modulation;

public class Request{


	protected Pair pair;
	protected Route route;
	protected double TimeOfRequestHours;
	protected double TimeOfFinalizeHours;
	protected double RequiredBandwidth;
	protected RequestGenerator rg;
	protected int spectrumAssigned[];
	protected Modulation modulation;
	
	/**
	 * @return the pair
	 */
	public Pair getPair() {
		return pair;
	}
	/**
	 * @param pair the pair to set
	 */
	public void setPair(Pair pair) {
		this.pair = pair;
	}
	/**
	 * @return the route
	 */
	public Route getRoute() {
		return route;
	}
	/**
	 * @param route the route to set
	 */
	public void setRoute(Route route) {
		this.route = route;
	}
	/**
	 * @return the setTimeOfRequestHours
	 */
	public double getTimeOfRequestHours() {
		return TimeOfRequestHours;
	}
	/**
	 * @param setTimeOfRequestHours the setTimeOfRequestHours to set
	 */
	public void setTimeOfRequestHours(double setTimeOfRequestHours) {
		this.TimeOfRequestHours = setTimeOfRequestHours;
	}
	/**
	 * @return the timeOfFinalizeHours
	 */
	public double getTimeOfFinalizeHours() {
		return TimeOfFinalizeHours;
	}
	/**
	 * @param timeOfFinalizeHours the timeOfFinalizeHours to set
	 */
	public void setTimeOfFinalizeHours(double timeOfFinalizeHours) {
		TimeOfFinalizeHours = timeOfFinalizeHours;
	}
	
	
	
	/**
	 * @return the requiredBandwidth
	 */
	public double getRequiredBandwidth() {
		return RequiredBandwidth;
	}
	/**
	 * @param requiredBandwidth the requiredBandwidth to set
	 */
	public void setRequiredBandwidth(double requiredBandwidth) {
		RequiredBandwidth = requiredBandwidth;
	}
	public Node getSource() {
		return pair.getSource();
	}
	
	public Node getDestination() {
		return pair.getDestination();
	}

	public RequestGenerator getRequestGenerator() {
		
		return rg;
	}
	
	public void setRequestGenerator(RequestGenerator rg) {
		this.rg = rg;		
	}
	
	public int[] getSpectrumAssigned() {
		return spectrumAssigned;
	}
	
	public void setSpectrumAssigned(int sa[]) {
		spectrumAssigned = sa;
	}
	/**
	 * @return the modulation
	 */
	public Modulation getModulation() {
		return modulation;
	}
	/**
	 * @param modulation the modulation to set
	 */
	public void setModulation(Modulation modulation) {
		this.modulation = modulation;
	}
	
	
	
	

}
