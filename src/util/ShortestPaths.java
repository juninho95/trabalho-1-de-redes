package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import network.Mesh;
import network.Node;
import rsa.Route;

public class ShortestPaths {

	
	public static HashMap<String, ArrayList<Route>> calculaMenoresRotas(Mesh mesh){
		
		HashMap<String, ArrayList<Route>> routesForAllPairs = new HashMap<String, ArrayList<Route>>();
		Iterator<Node> temp = mesh.getNodeList().iterator();
		Iterator<Node> temp2;
		Node or, des;
		
		while(temp.hasNext()){
			or = temp.next();
			temp2 = mesh.getNodeList().iterator();
			while(temp2.hasNext()){
				des = temp2.next();
				if(or != des){
					routesForAllPairs.put(or.getName() + "-" + des.getName(), menoresRotas(or, des, mesh));
				}
				
			}
		
		}
		
		return routesForAllPairs;
	}

	private static ArrayList<Route> menoresRotas(Node or, Node des,
			Mesh mesh) {
		ArrayList<Route> rotas = new ArrayList<Route>();
		HashSet<Node> nos = new HashSet<>(mesh.getNodeList());
		int tamanho = 99999999;
		
		nos.remove(or);
		Vector<Vector<Node>> pilhaBusca = new Vector<>();
		Vector<Node> alAux = new Vector<>();
		Vector<Node> alAux2;
		
		alAux.add(or);
		pilhaBusca.add(alAux);
		Node sAux;
		while(!pilhaBusca.isEmpty()){
			
			alAux = pilhaBusca.remove(pilhaBusca.size()-1);
			if(alAux.get(alAux.size()-1).equals(des)){
				if(alAux.size()<tamanho){
					tamanho = alAux.size();
					rotas = new ArrayList<>();
				}
				rotas.add(new Route(alAux));
				continue;
			}
			if(alAux.size() >= tamanho){
				continue;
			}
			
			sAux = alAux.get(alAux.size()-1);
			nos = new HashSet<>(mesh.getAdjacents(sAux));
			nos.removeAll(alAux);
			Iterator<Node> it = nos.iterator();
			
			while(it.hasNext()){
				alAux2 = (Vector<Node>)alAux.clone();
				sAux = it.next();
				alAux2.add(sAux);
				pilhaBusca.add(alAux2);
			}
		}
		return rotas;
	}
}
