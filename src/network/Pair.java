package network;

import java.util.*;

import java.io.Serializable;

import simulationControl.Util;


public class Pair implements Serializable {

  private Node source;
  private Node destination;
  private int id;
  private int numReqGenerated;
  private int numReqBlocked;
  private List<RequestGenerator> requestGenerators;

  /**
   * Cria instancia de Pair
   * @param s Node
   * @param d Node
   */
  public Pair(Node s, Node d) {
    this.source = s;
    this.destination = d;
    requestGenerators = new ArrayList<RequestGenerator>();    
  }

  //----------------------------------------------------------------------------
  /**
   * configura id
   * @param id int
   */
  public void setId(int id) {
    this.id = id;
  }

  //----------------------------------------------------------------------------
  /**
   * retorna o id
   * @return int
   */
  public int getId() {
    return this.id;
  }

  
  //----------------------------------------------------------------------------
  /**
   * retorna o source
   * @return Node
   */
  public Node getSource() {
    return this.source;
  }

  //----------------------------------------------------------------------------
  /**
   * retorna o numero de requisições geradas por este par
   * @return double
   */
  public double getNumReqGenerated() {
    
    return numReqGenerated;
  }

  //----------------------------------------------------------------------------
  /**
   * retorna o numero de bloqueios do par
   * @return double
   */
  public double getNumReqBlocked() {
    
    return numReqBlocked;
  }

 
  /**
 * @param numReqGenerated the numReqGenerated to set
 */
public void setNumReqGenerated(int numReqGenerated) {
	this.numReqGenerated = numReqGenerated;
}

/**
 * @param numReqBlocked the numReqBlocked to set
 */
public void setNumReqBlocked(int numReqBlocked) {
	this.numReqBlocked = numReqBlocked;
}

//----------------------------------------------------------------------------
  /**
   * retorna o destino
   * @return Node
   */
  public Node getDestination() {
    return this.destination;
  }

  

  //----------------------------------------------------------------------------
  /**
   * Retorna o nome do par
   * @return String
   */
  public String getPairName() {
    return "(" + this.source.getName() + "," + this.destination.getName() + ")";
  }

  //----------------------------------------------------------------------------
  /**
   * exibe o bloqueio do par na console
   * @return String
   */
  public String getBlockPair() {
    String s = "";
    s += ("Pb pair (" + this.source.getName() + "," +
          this.destination.getName() +
          ") =" + this.getNumReqBlocked() / this.getNumReqGenerated());
    // s+=("numero de vezes que o par foi gerado " + this.getNumReqGenerated());
    return s;
  }

  //----------------------------------------------------------------------------
  /**
   * Retorna o nome deste par:(o,d).
   * @return Object
   */
  public String getName() {
    return ("(" + this.source.getName() + "," + this.destination.getName() +
            ")");
  }

  //----------------------------------------------------------------------------
  /**
   * Retorna a probabilidade de bloqueio deste par.
   * @return double
   */
  public double getPbPair() {
    double getNumReqGenerated = this.getNumReqGenerated();
    if (getNumReqGenerated == 0) {
      return 0;
    }
    return (this.getNumReqBlocked() / getNumReqGenerated);
  }

  /**
   * adiciona um gerador de requisiçõe ao par
   * @param rg
   */
  public void addRequestGenerator(RequestGenerator rg){
	  requestGenerators.add(rg);
	  Util.bandwidths.add(rg.getBandwidth()); //utilizado na hora de gravar em arquivo os resultados da simulação
  }

  /**
   * @return the requestGenerators
   */
  public List<RequestGenerator> getRequestGenerators() {
	return requestGenerators;
  }
  
  public Pair clone(){
	  Pair p = new Pair(source, destination);
	  p.id = this.id;
	  p.numReqBlocked = this.numReqBlocked;
	  p.numReqGenerated = this.numReqGenerated;
	  p.requestGenerators = new ArrayList<RequestGenerator>();
	  
	  for (RequestGenerator rg : this.requestGenerators) {
		  p.requestGenerators.add(new RequestGenerator(p, rg.getBandwidth(), rg.getHoldRate(), rg.getArrivedRate(), rg.getIncLoad()));
	  }
	  
	  
	  return p;
  }
  
  @Override
  public boolean equals(Object o){
	  try{
		  System.out.println("ei");
		  Pair p = (Pair) o;
		  return (this.getSource().equals(p.source) && this.getDestination().equals(p.getDestination()));
		  
	  }catch(Exception ex){
		  return false;
	  }
  }


}
