package network;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import request.Request;
import rsa.RSA;
import rsa.Route;

/**
 * Classe que representa o plano de controle.
 * Esta classe dever� fazer as chamadas para os algoritmos RSA, armazenar rotas
 * em caso de roteamento fixo, prover informa��es a respeito do estado da rede, etc
 * @author Iallen
 *
 */
public class ControlPlane {
	
	private static ControlPlane singleton;
	
	private Mesh mesh;
	
	private RSA rsa;
	
	private ControlPlane(){		
	}
	
	/**
	 * m�todo para acessar a �nica inst�ncia de ControlPlane existente (padr�o singleton)
	 * @return
	 */
	public static ControlPlane getControlPlane(){
		if(singleton==null) singleton = new ControlPlane();
		
		return singleton;
	}
	
	
	
	/**
	 * @param mesh the mesh to set
	 */
	public void setMesh(Mesh mesh) {
		this.mesh = mesh;
	}
	
	

	/**
	 * @return the mesh
	 */
	public Mesh getMesh() {
		return mesh;
	}

	/**
	 * @param rsa the rsa to set
	 */
	public void setRsa(RSA rsa) {
		this.rsa = rsa;
	}

	/**
	 * Retorna as rotas para cada par(o,d) na rede
	 * m�todo utilizado apenas para roteamento fixo
	 * @return
	 */
	public Vector<Route> getRoutesForAllPairs(){
		return rsa.getRoutesForAllPairs();
	}
	
	
	/**
	 * Este m�todo tenta atender uma determinada requisi��o
	 * alocando recursos caso dispon�vel e estabelecer o circuito
	 * @param request
	 * @return
	 */
	public boolean atenderRequisicao(Request request){
		if(this.rsa.atenderRequisicao(request)){
			
			//colocar a verificacao de QoT aqui
			
			allocarRecursos(request);
			
			return true;
		}
		return false; 
	}

	/**
	 * libera os recursos que est�o sendo utilizados por um determinado circuito
	 * @param request
	 */
	public void freeResources(Request request) {
		Route r = request.getRoute();
		
		for (Link link : r.getLinkList()) {
			link.liberateSpectrum(request.getSpectrumAssigned());
		}
		
	}
	
	/**
	 * Este metodo eh chamado apos a execucao dos algoritmos RSA para fazer a alocacao de recursos na rede
	 * @param request
	 */
	private void allocarRecursos(Request request){
		Route route = request.getRoute();		
		List<Link> links = new ArrayList<>(route.getLinkList());
		int chosen[] = request.getSpectrumAssigned();
		
		boolean notAbleAnymore = false;
		Link l;
		int i;
		for (i=0;i<links.size();i++) {
			l = links.get(i);
			notAbleAnymore = !l.useSpectrum(chosen);
			if(notAbleAnymore) break; //algum recurso n�o estava mais dispon�vel,cancelar a aloca��o	
			
		}
		
		if(notAbleAnymore){ //liberar os recursos alocados caso n�o seja poss�vel estabelecer o circuito
			for (i=i-1;i>=0;i--) {
				l = links.get(i);
				l.liberateSpectrum(chosen);				
			}
		}
		
	}
}
