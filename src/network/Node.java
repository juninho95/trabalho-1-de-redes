package network;

import java.io.*;
import java.util.*;

import request.*;
import simulator.*;

public class Node implements Serializable {

    private String name;
    private Oxc oxc;
    private int trafficWeigth;
    private Vector<Pair> pairs;
    private Transmitters txs;
    private Receivers rxs;
    private double numberReqGenerated;
    private double distanceToCentralNodeControlPlane = -9999999;

    /**
     * Creates a new instance of Node.
     * @param name String
     */
    public Node(String name) {
        this.name = name;
        this.oxc = new Oxc(name);
        this.trafficWeigth = 1;
        this.txs = new Transmitters();
        this.rxs = new Receivers();
        this.numberReqGenerated = 0;
    }

    /**
     * Creates a new instance of Node.
     * @param name String
     * @param reqQueue ANodeConnectionQueue
     * @param weigth int
     */
    public Node(String name, int weigth) {
        this.name = name;
        this.trafficWeigth = weigth;
        this.oxc = new Oxc(name);
        this.txs = new Transmitters();
        this.rxs = new Receivers();
        this.numberReqGenerated = 0;
    }

    public boolean equals(Object o) {
        if (o instanceof Node) {
            if (name.equals(((Node) o).getName())) {
                return true;
            }
        }
        return false;
    }




//------------------------------------------------------------------------------
    /**
     * Getter for property oxc
     * @return Oxc oxc this Node.
     */
    public Oxc getOxc() {
        return this.oxc;
    }

//------------------------------------------------------------------------------
    /**
     * Getter for property name
     * @return String name name this Node.
     */
    public String getName() {
        return name;
    }

//------------------------------------------------------------------------------
    /**
     * Retorna o privilegio do n�
     * @return int
     */
    public int getWeigth() {
        return this.trafficWeigth;
    }

    //------------------------------------------------------------------------------
   

    

    //------------------------------------------------------------------------------
    /**
     * Retorna os pares cuja origem e este no.
     * @return Vector
     */
    public Vector getPairs() {
        return pairs;
    }


    //------------------------------------------------------------------------------
    /**
     * Retorna o banco de transmissores deste No
     * @return Transmitters
     */
    public Transmitters getTxs() {
        return txs;
    }

    //------------------------------------------------------------------------------
    /**
     * Retorna o banco de Receptores deste No
     * @return Receivers
     */
    public Receivers getRxs() {
        return rxs;
    }

    //------------------------------------------------------------------------------
    /**
     * Retorna o numero de requisicoes geradas com origem neste No
     * @return double
     */
    public double getNumberReqGenerated() {
        return numberReqGenerated;
    }

    //------------------------------------------------------------------------------
    /**
     * Retorna a distancia para deste no para o No Central do Plano de Controle
     * @return double
     */
    public double getDistanceToCentralNodeControlPlane() {
      return distanceToCentralNodeControlPlane;
  }

  //------------------------------------------------------------------------------
    /**
     * configura o peso do n�
     * @param weigth int
     */
    public void setTrafficWeigth(int weigth) {
        this.trafficWeigth = weigth;
    }


    //------------------------------------------------------------------------------
    /**
     * Configura os pares cuja origem e este no
     * @param pairs Vector
     */
    public void setPairs(Vector pairs) {
        this.pairs = pairs;
    }


  //------------------------------------------------------------------------------
  /**
   * Configura a distancia para deste no para o No Central do Plano de Controle
   * @param distanceToCentralNodeControlPlane double
   */
  public void setDistanceToCentralNodeControlPlane(double distanceToCentralNodeControlPlane) {
    this.distanceToCentralNodeControlPlane = distanceToCentralNodeControlPlane;

  }

  //------------------------------------------------------------------------------
    /**
     * Incrementa o numero de requisicoes geradas com origem neste No
     */
    public void incNumberReqGenerated() {
        this.numberReqGenerated++;
    }

    //------------------------------------------------------------------------------
    



    //------------------------------------------------------------------------------
    /**
     * retorna true se o n� for um WCR e possuir conversores livres. Caso contr�rio
     * retorna false.
     * @return boolean
     */
    public boolean canConverter() {
        return false;
    }

    //----------------------------------------------------------------------------
    /**
     * Retorna um par aleatorio com origem neste no. getRandomPair
     * @return Pair
     * @param randomVar RandGenerator
     */
    public Pair getRandomPair(RandGenerator randomVar) {
        int index = randomVar.randInt(0, this.pairs.size() - 1);
        return this.pairs.get(index);
    }

    /**
     * finish
     * @param timeHours double
     */
    public void finish(double timeHours) {
       // this.reqQueue.finish(timeHours);
    }

    //restart
    public void reStart() {

        this.txs.reStart();
        this.rxs.reStart();
     

    }

}
