package network;

import request.*;
import simulator.*;
import simulator.eventListeners.ArriveRequestListener;

import java.io.Serializable;

public class RequestGenerator implements Serializable {

  private Pair pair;
  private double bandwidth; // em bits por segundo (bps)
  private double holdRate;
  private double arrivedRate;  
  private double incLoad;
  private double atualTimeHours;
  
  

  /**
   * Construtor padrao
   */
  public RequestGenerator() {
    
  }

  /**
   * 
   * @param pair
   * @param bandwidth em bits por segundo (bps)
   * @param holdRate
   * @param arrivedRate
   * @param incLoad
   */
  public RequestGenerator(Pair pair, double bandwidth, double holdRate, double arrivedRate,double incLoad) {
    this.pair = pair;
    this.bandwidth = bandwidth;	  
	this.holdRate = holdRate;
    this.arrivedRate = arrivedRate;
    this.incLoad=incLoad;
    atualTimeHours = 0;
  }

  //---------------------------------------------------------------------------
  /**
   * Agenda uma nova requisicao de conexao,
   * o m�todo ir� calcular o instante da pr�xima requisi��o e agendar o evento correspondente
   * @param r RequestMother
   * @param atualTimeHours double
   */
  public void scheduleNextRequest(EventMachine em, ArriveRequestListener arriveRequest) {
	Request r = new Request();
	double arriveTimeHours = ControlPlane.getControlPlane().getMesh().getRandomVar().negexp(arrivedRate);
    atualTimeHours = atualTimeHours + arriveTimeHours;
    r.setTimeOfRequestHours(atualTimeHours);
    r.setPair(pair);
    r.setRequiredBandwidth(bandwidth);
    r.setRequestGenerator(this);
    Event e = new Event(r, arriveRequest, atualTimeHours);
    em.insert(e);
  }

  //---------------------------------------------------------------------------
  /**
   * Retorna a taxa de chegada de requisicoes
   * @return double
   */
  public double getArrivedRate() {
    return arrivedRate;
  }

 

  //---------------------------------------------------------------------------
  /**
   * Retorna a taxa de atendimento de requisicoes.
   * @return double
   */
  public double getHoldRate() {
    return holdRate;
  }

 

  //---------------------------------------------------------------------------
  /**
   * Configura a taxa de chegada de requisicoes.
   * @param arrivedRate double
   */
  public void setArrivedRate(double arrivedRate) {
    this.arrivedRate = arrivedRate;
  }



  //---------------------------------------------------------------------------
  /**
   * Configura a taxa de atendimento de requisicoes.
   * @param holdRate double
   */
  public void setHoldRate(double holdRate) {
    this.holdRate = holdRate;
  }

  

  //---------------------------------------------------------------------------





  //---------------------------------------------------------------------------
  /**
   * Retorna o valor de incremento para a taxa de chegada.
   * @return double
   */
  public double getIncLoad() {
    return incLoad;
  }


  //---------------------------------------------------------------------------
  /**
   * Configura o valor de incremento para a taxa de chegada.
   * @param incLoad double
   */
  public void setIncLoad(double incLoad) {
    this.incLoad = incLoad;
  }

/**
 * @return the pair
 */
public Pair getPair() {
	return pair;
}

/**
 * @param pair the pair to set
 */
public void setPair(Pair pair) {
	this.pair = pair;
}

/**
 * @return the bandwidth
 */
public double getBandwidth() {
	return bandwidth;
}

/**
 * @param bandwidth the bandwidth to set
 */
public void setBandwidth(double bandwidth) {
	this.bandwidth = bandwidth;
}

public void incArrivedRate(){
	this.arrivedRate = this.arrivedRate + this.incLoad;
}


  
  
}
