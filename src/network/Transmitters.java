package network;

import java.io.Serializable;

public class Transmitters implements Serializable {

  private double txUtilization;
  private double sumTxUtilization;
  private double picoTxUtilization;
  private double numConnectionsOrigem;

  public Transmitters() {
    this.txUtilization = 0.0;
    this.sumTxUtilization = 0.0;
    this.picoTxUtilization = 0.0;
    this.numConnectionsOrigem=0.0;
  }
  //----------------------------------------------------------------------------
  /**
   * Incrementa o num de transmissores que estao sendo utilizados e
   * atualiza o somatorio e o pico de utilizacao de receptor
   */
  public void incTxUtilization() {
    this.txUtilization++;
    this.sumTxUtilization += this.txUtilization;

    if (this.picoTxUtilization < this.txUtilization) {
      this.picoTxUtilization = this.txUtilization;
    }
    numConnectionsOrigem++;
  }
  //----------------------------------------------------------------------------
  /**
   * Decrementa o num de transmissores que estao sendo utilizados
   * Este metodo dever ser invocado quando uma conexao com
   * origem neste ns for finalizada
   */
  public void subTxUtilization() {
    this.txUtilization--;
  }
  //----------------------------------------------------------------------------
  /**
   * Retorna a media da utilizacao deste transmissor
   * @return double
   */
  public double getTxUtilizationAvg() {
    return (this.sumTxUtilization / this.numConnectionsOrigem);
  }

  //----------------------------------------------------------------------------
  /**
   * Retorna o pico de utilizacao deste transmissor;
   * @return double
   */
  public double getPicoTxUtilization() {
    return picoTxUtilization;
  }

  /**
   * restart
   */
  public void reStart() {
    this.sumTxUtilization = 0.0;
    this.picoTxUtilization = 0.0;
    this.numConnectionsOrigem=0.0;
  }

}
