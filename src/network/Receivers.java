package network;

import java.io.Serializable;

public class Receivers implements Serializable {

  private double rxUtilization;
  private double sumRxUtilization;
  private double picoRxUtilization;
  private double numConnectionsDestino;

  public Receivers() {
    this.rxUtilization = 0.0;
    this.sumRxUtilization = 0.0;
    this.picoRxUtilization = 0.0;
    this.numConnectionsDestino=0.0;
  }
  //----------------------------------------------------------------------------
  /**
   * Incrementa o num de receptores que estao sendo utilizados e
   * atualiza o somatorio e o pico de utilizacao de receptor
   */
  public void incRxUtilization() {
    this.rxUtilization++;
    this.sumRxUtilization += this.rxUtilization;

    if (this.picoRxUtilization < this.rxUtilization) {
      this.picoRxUtilization = this.rxUtilization;
    }
    this.numConnectionsDestino++;
  }

  //----------------------------------------------------------------------------
  /**
   * Decrementa o num de receptores que estao sendo utilizados
   */
  public void subRxUtilization() {
    this.rxUtilization--;
  }
  //----------------------------------------------------------------------------
  /**
   * Retorna a media da utilizacao de receptores deste No
   * @return double
   */
  public double getRxUtilizationAvg() {
    return (this.sumRxUtilization / this.numConnectionsDestino);
  }
  //----------------------------------------------------------------------------
  /**
   * Retorna o pico de utilizacao de receptores deste No;
   * @return double
   */
  public double getPicoRxUtilization() {
    return picoRxUtilization;
  }

  /**
   * restart
   */
  public void reStart() {
    this.sumRxUtilization = 0.0;
    this.picoRxUtilization = 0.0;
    this.numConnectionsDestino=0.0;
  }

}
