package network;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import measurement.Measurements;
import rsa.RSA;
import simulator.RandGenerator;

public class Mesh implements Serializable {

    private Vector<Node> nodeList;
    private Measurements measurements;
    private RandGenerator randomVar;
    private Vector<Link> linkList;
    private int conversionType;
    private Vector<Pair> pairList;
    
    
    

    public Mesh(Vector<Node> nodeList, Vector<Pair> pairList, String routingAlgo, String spectrumAssignmentAlgo) {
        this.nodeList = nodeList;
        this.randomVar = new RandGenerator();        
        this.coumputeAllLinks();
        this.pairList = pairList;
        try {
			ControlPlane.getControlPlane().setRsa(new RSA(routingAlgo, spectrumAssignmentAlgo, this.linkList.get(0).getSlotSpectrumBand()));//tipo de roteamento e aloca��o de comprimento de onda fixos por enquanto
		} catch (Exception e) {
			// roteamento ou aloca��o de comprimento de onda desconhecidos
			e.printStackTrace();
		} 
        
    }
    
    public Mesh(Vector<Node> nodeList, Vector<Pair> pairList, String integratedAlgo) {
        this.nodeList = nodeList;
        this.randomVar = new RandGenerator();        
        this.coumputeAllLinks();
        this.pairList = pairList;
        try {
			ControlPlane.getControlPlane().setRsa(new RSA(integratedAlgo, this.linkList.get(0).getSlotSpectrumBand()));//tipo de roteamento e aloca��o de comprimento de onda fixos por enquanto
		} catch (Exception e) {
			// roteamento ou aloca��o de comprimento de onda desconhecidos
			e.printStackTrace();
		} 
        
    }



  



    public Link getLink(String source, String destination) {
        for (int i = 0; i < linkList.size(); i++) {
            if ((linkList.get(i).getSource().getName() == source) &&
                    (linkList.get(i).getDestination().getName() == destination)) {
                return linkList.get(i);
            }

        }
        return null;
    }

    /**
     * retorna um Vector com todos os enlaces.
     * @return Vector
     */
    public Vector<Link> getLinkList() {
        return linkList;
    }

    /**
     * computa todos os enlaces e armazena os enlaces em linkList.
     */
    private void coumputeAllLinks() {
        linkList = new Vector<Link>();
        for (int i = 0; i < nodeList.size(); i++) {
            linkList.addAll(nodeList.get(i).getOxc().getLinksList());
        }
    }

    /**
     * retorna um enlace sorteado aleatoriamente dentre todos os enlaces poss�veis.
     * Assume que o enlace � bidirecional. Isto �, o m�todo retorna o enlace de
     * ida e o enlace de volta.
     * @return Link
     */
    public Vector<Link> getRandomOneLink() {
        Vector<Link> listLinks = new Vector<Link>();
        int x = this.getRandomVar().randInt(0, this.getLinkList().size() - 1);
        Link auxLink = this.linkList.get(x);
        //ida
        listLinks.add(auxLink);
        //volta
        listLinks.add(this.getLink(auxLink.getDestination().getName(),
                auxLink.getSource().getName()));
        return listLinks;

    }

   

//------------------------------------------------------------------------------
    public Measurements getMeasurements() {
        return this.measurements;
    }

//------------------------------------------------------------------------------
    public RandGenerator getRandomVar() {
        return this.randomVar;
    }


//------------------------------------------------------------------------------
    /**
     * Add Node in property nodeList.
     * @param x Node
     * @return true if Node x added successfully; false otherwise.
     */
    public boolean addNode(Node x) {
        return nodeList.add(x);
    }

//------------------------------------------------------------------------------
    /**
     * Getter for property nodeList.
     * @return Vector with nodes.
     */
    public Vector<Node> getNodeList() {
        return nodeList;
    }

//------------------------------------------------------------------------------
    /**
     * get Node index
     * @param index int
     * @return Node Node i.
     */
    public Node getNode(int index) {
        return nodeList.get(index);
    }

    //------------------------------------------------------------------------------
   

    //------------------------------------------------------------------------------
    /**
     * Localiza um Node em fun��o do nome.
     * @param name String
     * @return Node
     */
    public Node searchNode(String name) {
        for (int i = 0; i < this.nodeList.size(); i++) {
            Node tmp = nodeList.get(i);
            if (tmp.getName().equals(name)) {
                return tmp;
            }
        }
        return null;
    }



//------------------------------------------------------------------------------
    public void setNodeList(Vector nodeList) {
        this.nodeList = nodeList;
    }



    //----------------------------------------------------------------------------

    //----------------------------------------------------------------------------
    /**
     * Invocado na finalizacao da simulacao
     * Configura metricas relacionadas as requisicoes que ficaram na fila de requisicoes
     * @param timeHours double
     */
    public void finish(double timeHours) {
        for (int i = 0; i < nodeList.size(); i++) {
            nodeList.get(i).finish(timeHours);
        }
    }


    /**
     * retorna os n�s ating�veis a partir de um determinado n�
     * @param n
     * @return
     */
    public List<Node> getAdjacents(Node n){
    	List<Node> res = new ArrayList<>();
    	for (Oxc o : n.getOxc().getAllAdjacents()) {
			res.add(searchNode(o.getName()));
		}
    	
    	return res;
    }

    

	/**
	 * @return the pairList
	 */
	public Vector<Pair> getPairList() {
		return pairList;
	}



	public void setMeasurements(Measurements measurements) {
		this.measurements = measurements;		
	}
    
    
}
