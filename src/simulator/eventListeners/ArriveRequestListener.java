package simulator.eventListeners;

import java.io.Serializable;

import measurement.Measurements;
import network.ControlPlane;
import request.Request;

import simulator.Event;
import simulator.EventMachine;

/**
 * Esta classe � o escutador do evento de uma chegada de uma requisi��o.
 * A execu��o destes eventos deve tentar atender as requisi��es executando os algoritmos RSA
 * @author Iallen
 *
 */
public class ArriveRequestListener implements EventListener, Serializable  {

	private EventMachine em;
	
	public ArriveRequestListener(EventMachine em){
		this.em = em;
	}
	
	@Override
	public void execute(Event e) {
		
		Request request = (Request) e.getObject();
		
		beforeReq(request);
		
		//calcular o tempo de dura��o da requisi��o
		double holdTimeHours = ControlPlane.getControlPlane().getMesh().getRandomVar().negexp(request.getRequestGenerator().getHoldRate());			
		double finalizeTimeHours = request.getTimeOfRequestHours() + holdTimeHours;
		request.setTimeOfFinalizeHours(finalizeTimeHours);
		
		//tentar atender a requisi��o
		Boolean success = ControlPlane.getControlPlane().atenderRequisicao(request);
		if(success){//agendar o fim do circuito e libera��o dos recursos
			//System.out.println("espectro alocado: " + request.getSpectrumAssigned()[0] + "-" + request.getSpectrumAssigned()[1]);
			
			em.insert(new Event(request, new HoldRequestListener(), finalizeTimeHours));			
		}
		
		
		//atualizar as m�tricas de desempenho
		afterReq(request, success);
				
	}
	
	private void beforeReq(Request request){
		Measurements m = ControlPlane.getControlPlane().getMesh().getMeasurements();
		
		m.transientStepVerify(ControlPlane.getControlPlane().getMesh().getNodeList());
		
		m.incNumGeneratedReq();
		
		
	}

	private void afterReq(Request request, boolean success){
		Measurements m = ControlPlane.getControlPlane().getMesh().getMeasurements();
		
		m.getProbabilidadeDeBloqueioMeasurement().addNewObservation(success, request);
		m.getProbabilidadeDeBloqueioDeBandaMeasurement().addNewObservation(success, request);
		m.getFragmentacaoExterna().addNewObservation(request);
		m.getUtilizacaoSpectro().addNewObservation(request);
		m.getFragmentacaoRelativa().addNewObservation(request);
		
		if(!m.finished()){ //agendar outra requisi��o atrav�s do mesmo gerador desta
			request.getRequestGenerator().scheduleNextRequest(em, this);
		}
		
	}
	
	
	
}
