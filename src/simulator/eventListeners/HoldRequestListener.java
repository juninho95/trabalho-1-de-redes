package simulator.eventListeners;

import java.io.Serializable;

import network.ControlPlane;
import request.Request;

import simulator.Event;

public class HoldRequestListener implements EventListener, Serializable {

	@Override
	public void execute(Event e) {
		Request request = (Request) e.getObject();
		ControlPlane.getControlPlane().freeResources(request);
	}

}
