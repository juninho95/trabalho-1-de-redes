package simulator;

import network.*;
import java.io.Serializable;

import rsa.RSA;

public class Simulation implements Serializable {

    
    private int numReply;    
    private int totalNumberOfRequests;
    private int rsaType;
    private String rountingAlgorithm;
    private String spectrumAssignmentAlgorithm;
    private String integratedRSAAlgorithm;
    private Mesh mesh;

   
    public Simulation() {
        
      
        
    }


	/**
	 * @return the numReply
	 */
	public int getNumReply() {
		return numReply;
	}


	/**
	 * @param numReply the numReply to set
	 */
	public void setNumReply(int numReply) {
		this.numReply = numReply;
	}


	/**
	 * @return the totalNumberOfRequests
	 */
	public int getTotalNumberOfRequests() {
		return totalNumberOfRequests;
	}


	/**
	 * @param totalNumberOfRequests the totalNumberOfRequests to set
	 */
	public void setTotalNumberOfRequests(int totalNumberOfRequests) {
		this.totalNumberOfRequests = totalNumberOfRequests;
	}


	/**
	 * @return the rsaType
	 */
	public int getRsaType() {
		return rsaType;
	}


	/**
	 * @param rsaType the rsaType to set
	 */
	public void setRsaType(int rsaType) {
		this.rsaType = rsaType;
	}


	/**
	 * @return the rountingAlgorithm
	 */
	public String getRountingAlgorithm() {
		return rountingAlgorithm;
	}


	/**
	 * @param rountingAlgorithm the rountingAlgorithm to set
	 * @throws Exception 
	 */
	public void setRountingAlgorithm(String rountingAlgorithm) throws Exception {
		this.rountingAlgorithm = rountingAlgorithm;
		
	}


	/**
	 * @return the spectrumAssignmentAlgorithm
	 */
	public String getSpectrumAssignmentAlgorithm() {
		return spectrumAssignmentAlgorithm;
	}


	/**
	 * @param spectrumAssignmentAlgorithm the spectrumAssignmentAlgorithm to set
	 * @throws Exception 
	 */
	public void setSpectrumAssignmentAlgorithm(String spectrumAssignmentAlgorithm) throws Exception {
		this.spectrumAssignmentAlgorithm = spectrumAssignmentAlgorithm;
		
	}


	/**
	 * @return the integratedRSAAlgorithm
	 */
	public String getIntegratedRSAAlgorithm() {
		return integratedRSAAlgorithm;
	}


	/**
	 * @param integratedRSAAlgorithm the integratedRSAAlgorithm to set
	 * @throws Exception 
	 */
	public void setIntegratedRSAAlgorithm(String integratedRSAAlgorithm) throws Exception {
		
		this.integratedRSAAlgorithm = integratedRSAAlgorithm;
	}


	/**
	 * @return the mesh
	 */
	public Mesh getMesh() {
		return mesh;
	}


	/**
	 * @param mesh the mesh to set
	 */
	public void setMesh(Mesh mesh) {
		this.mesh = mesh;
	}



}
