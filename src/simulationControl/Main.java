package simulationControl;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import measurement.Measurements;
import network.Mesh;
import network.Node;
import network.Pair;
import network.RequestGenerator;
import rsa.RSA;
import simulationControl.parsers.NetworkFileReader;
import simulationControl.parsers.SimulationFileReader;
import simulationControl.parsers.TraficFileReader;
import simulator.Simulation;

/**
 * esta classe possui o m�todo main que ir� instanciar os parsers para leitura dos arquivos de configura��o e iniciar a simula��o
 * @author Iallen
 *
 */
public class Main {
	
	private static NetworkFileReader nfr;
	private static TraficFileReader tfr;
	private static SimulationFileReader sfr;

	/**
	 * @param args
	 * arg[0] - > path dos arquivos de configura��o
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Reading files");
		List<List<Simulation>> allSimulations = createAllSimulations(args);
		
		//agora dar o start nas simula��es	
		System.out.println("Starting simulations");
		SimulationManagement sm = new SimulationManagement(allSimulations, args[0]);
		sm.startSimulations();
		System.out.println("saving results");
		sm.saveResults();
		System.out.println("finish!");
		
	}

	
	private static List<List<Simulation>> createAllSimulations(String[] args) throws Exception{
		
		//path dos arquivos de configura��o da simula��o
		String filesPath = args[0];
		String networkFilePath = filesPath + "/network";
		String simulationFilePath = filesPath + "/simulation";
		String traficFilePath = filesPath + "/trafic";
		String routesFilePath = filesPath + "/fixedRoutes";
			
		Util.projectPath = filesPath;
		//ler o arquivo network
		nfr = new NetworkFileReader();
		Vector<Node> nodes = nfr.readNetwork(networkFilePath);
		
		//ler o arquivo trafic
		tfr = new TraficFileReader();
		Vector<Pair> p = tfr.readTrafic(traficFilePath, nodes);
		
		//ler o arquivo simulation	
		List<List<Simulation>> allSimulations = new ArrayList<List<Simulation>>(); // cada elemento deste conjunto � uma lista com 10 replica��es de um mesmo ponto de carga
		sfr = new SimulationFileReader(simulationFilePath);
		
		int i, j;
		
		
		for(i=0;i<sfr.getLoadPoints();i++){ //criar as simula��es para cada ponto de carga
			List<Simulation> reps = new ArrayList<Simulation>();
			p = clone(p);
			
			if(i!=0)incArrivedRate(p); //primeiro ponto de carga n�o � incrementado
			
			for(j=0;j<sfr.getReplications();j++){
				Simulation s = new Simulation();
				s.setIntegratedRSAAlgorithm(sfr.getRsaIntegratedAlgo());
				s.setNumReply(sfr.getReplications());
				s.setRountingAlgorithm(sfr.getRoutingAlgo());
				s.setRsaType(sfr.getRsaType());
				s.setSpectrumAssignmentAlgorithm(sfr.getSpectrumAssignAlgo());
				s.setTotalNumberOfRequests(sfr.getRequests());
				Vector<Node> n = (Vector<Node>) nodes.clone();
				Mesh m = null;
				switch(sfr.getRsaType()){
					case RSA.RSA_SEQUENCIAL:
						m = new Mesh(n, clone(p), sfr.getRoutingAlgo(), sfr.getSpectrumAssignAlgo());
						break;
					case RSA.RSA_INTEGRATED:
						m = new Mesh(n, clone(p), sfr.getRsaIntegratedAlgo());
						break;
				}
				
				m.setMeasurements(new Measurements(sfr.getRequests(), i+1, j+1));
				s.setMesh(m);
				reps.add(s);
				
			}
			allSimulations.add(reps);
		}
		
		return allSimulations;
		
	}
	
	/**
	 * este m�todo seta o ponto de carga da simula��o em cada gerador de requisi��es
	 * @param pairs
	 */
	private static void incArrivedRate(Vector<Pair> pairs){
		for (Pair pair : pairs) {
			for (RequestGenerator rg : pair.getRequestGenerators()) {
				rg.incArrivedRate();
			}
		}
	}
	
	private static Vector<Pair> clone(Vector<Pair> pairs){
		Vector<Pair> res = new Vector<>();
			for (Pair pair : pairs) {
				res.add(pair.clone());
			}
		return res;
	}
	
}
