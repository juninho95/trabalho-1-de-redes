// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g 2014-07-14 12:29:10

package simulationControl.parsers;
import java.util.HashMap;
import network.Node;
import network.Link;
import java.util.Vector;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class networkParserParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "INT", "FLOAT", "EXPONENT", "COMMENT", "'node:'", "'\\n'", "'links:'", "';'"
    };
    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int INT=4;
    public static final int FLOAT=5;
    public static final int EXPONENT=6;
    public static final int COMMENT=7;

    // delegates
    // delegators


        public networkParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public networkParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return networkParserParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g"; }


    private HashMap<String,Node> nodes;
    public Vector<Node> getNodes() {
    	return new Vector<Node>(nodes.values());
    }



    // $ANTLR start "initial"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:27:1: initial : nodesDescription linksDescription ;
    public final void initial() throws RecognitionException {

        	nodes = new HashMap<>();

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:31:2: ( nodesDescription linksDescription )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:33:2: nodesDescription linksDescription
            {
            pushFollow(FOLLOW_nodesDescription_in_initial45);
            nodesDescription();

            state._fsp--;

            pushFollow(FOLLOW_linksDescription_in_initial48);
            linksDescription();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "initial"


    // $ANTLR start "nodesDescription"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:38:1: nodesDescription : 'node:' '\\n' ( INT '\\n' )* ;
    public final void nodesDescription() throws RecognitionException {
        Token INT1=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:39:2: ( 'node:' '\\n' ( INT '\\n' )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:40:3: 'node:' '\\n' ( INT '\\n' )*
            {
            match(input,8,FOLLOW_8_in_nodesDescription63); 
            match(input,9,FOLLOW_9_in_nodesDescription65); 
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:40:15: ( INT '\\n' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==INT) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:40:16: INT '\\n'
            	    {
            	    INT1=(Token)match(input,INT,FOLLOW_INT_in_nodesDescription67); 

            	    				Node n = new Node((INT1!=null?INT1.getText():null));
            	    				nodes.put(n.getName(),n);		
            	    				
            	    match(input,9,FOLLOW_9_in_nodesDescription70); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "nodesDescription"


    // $ANTLR start "linksDescription"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:49:1: linksDescription : 'links:' '\\n' ( linkDescription '\\n' )* ;
    public final void linksDescription() throws RecognitionException {
        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:50:2: ( 'links:' '\\n' ( linkDescription '\\n' )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:51:2: 'links:' '\\n' ( linkDescription '\\n' )*
            {
            match(input,10,FOLLOW_10_in_linksDescription91); 
            match(input,9,FOLLOW_9_in_linksDescription93); 
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:52:2: ( linkDescription '\\n' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==INT) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:52:3: linkDescription '\\n'
            	    {
            	    pushFollow(FOLLOW_linkDescription_in_linksDescription97);
            	    linkDescription();

            	    state._fsp--;

            	    match(input,9,FOLLOW_9_in_linksDescription99); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "linksDescription"


    // $ANTLR start "linkDescription"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:56:1: linkDescription : source= INT ';' dest= INT ';' cost= FLOAT ';' slots= INT ';' slotLB= FLOAT ';' dist= INT ;
    public final void linkDescription() throws RecognitionException {
        Token source=null;
        Token dest=null;
        Token cost=null;
        Token slots=null;
        Token slotLB=null;
        Token dist=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:57:2: (source= INT ';' dest= INT ';' cost= FLOAT ';' slots= INT ';' slotLB= FLOAT ';' dist= INT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\networkParser.g:58:3: source= INT ';' dest= INT ';' cost= FLOAT ';' slots= INT ';' slotLB= FLOAT ';' dist= INT
            {
            source=(Token)match(input,INT,FOLLOW_INT_in_linkDescription121); 
            match(input,11,FOLLOW_11_in_linkDescription122); 
            dest=(Token)match(input,INT,FOLLOW_INT_in_linkDescription127); 
            match(input,11,FOLLOW_11_in_linkDescription128); 
            cost=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_linkDescription133); 
            match(input,11,FOLLOW_11_in_linkDescription134); 
            slots=(Token)match(input,INT,FOLLOW_INT_in_linkDescription139); 
            match(input,11,FOLLOW_11_in_linkDescription140); 
            slotLB=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_linkDescription145); 
            match(input,11,FOLLOW_11_in_linkDescription146); 
            dist=(Token)match(input,INT,FOLLOW_INT_in_linkDescription151); 

            			Node src = nodes.get((source!=null?source.getText():null));
            			Node d = nodes.get((dest!=null?dest.getText():null));
            			Double c = Double.parseDouble((cost!=null?cost.getText():null));
            			int s = Integer.parseInt((slots!=null?slots.getText():null));
            			Double slb = Double.parseDouble((slotLB!=null?slotLB.getText():null));
            			int dis = Integer.parseInt((dist!=null?dist.getText():null));
            			
            			src.getOxc().addLink(new Link(src.getOxc(), d.getOxc(), c, s, slb,dis));
            		
            		

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "linkDescription"

    // Delegated rules


 

    public static final BitSet FOLLOW_nodesDescription_in_initial45 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_linksDescription_in_initial48 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_8_in_nodesDescription63 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_nodesDescription65 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_INT_in_nodesDescription67 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_nodesDescription70 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_10_in_linksDescription91 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_linksDescription93 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_linkDescription_in_linksDescription97 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_linksDescription99 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_INT_in_linkDescription121 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_linkDescription122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INT_in_linkDescription127 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_linkDescription128 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_FLOAT_in_linkDescription133 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_linkDescription134 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INT_in_linkDescription139 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_linkDescription140 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_FLOAT_in_linkDescription145 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_linkDescription146 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INT_in_linkDescription151 = new BitSet(new long[]{0x0000000000000002L});

}