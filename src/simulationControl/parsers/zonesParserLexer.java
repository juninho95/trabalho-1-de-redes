// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g 2014-11-26 13:27:02

package simulationControl.parsers;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class zonesParserLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int INT=4;
    public static final int COMMENT=5;

    // delegates
    // delegators

    public zonesParserLexer() {;} 
    public zonesParserLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public zonesParserLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g"; }

    // $ANTLR start "T__6"
    public final void mT__6() throws RecognitionException {
        try {
            int _type = T__6;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:7:6: ( ':' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:7:8: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__6"

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:8:6: ( '<' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:8:8: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:9:6: ( '-' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:9:8: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:10:6: ( '>' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:10:8: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:38:5: ( ( '0' .. '9' )+ )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:38:7: ( '0' .. '9' )+
            {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:38:7: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:38:7: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' | '\\n' | '\\r' )
            int alt5=4;
            switch ( input.LA(1) ) {
            case '/':
                {
                int LA5_1 = input.LA(2);

                if ( (LA5_1=='/') ) {
                    alt5=1;
                }
                else if ( (LA5_1=='*') ) {
                    alt5=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
                }
                break;
            case '\n':
                {
                alt5=3;
                }
                break;
            case '\r':
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
                    {
                    match("//"); 

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:14: (~ ( '\\n' | '\\r' ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>='\u0000' && LA2_0<='\t')||(LA2_0>='\u000B' && LA2_0<='\f')||(LA2_0>='\u000E' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:14: ~ ( '\\n' | '\\r' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:28: ( '\\r' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0=='\r') ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:42:28: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 
                    _channel=HIDDEN;

                    }
                    break;
                case 2 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:43:9: '/*' ( options {greedy=false; } : . )* '*/'
                    {
                    match("/*"); 

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:43:14: ( options {greedy=false; } : . )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0=='*') ) {
                            int LA4_1 = input.LA(2);

                            if ( (LA4_1=='/') ) {
                                alt4=2;
                            }
                            else if ( ((LA4_1>='\u0000' && LA4_1<='.')||(LA4_1>='0' && LA4_1<='\uFFFF')) ) {
                                alt4=1;
                            }


                        }
                        else if ( ((LA4_0>='\u0000' && LA4_0<=')')||(LA4_0>='+' && LA4_0<='\uFFFF')) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:43:42: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    match("*/"); 

                    _channel=HIDDEN;

                    }
                    break;
                case 3 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:44:9: '\\n'
                    {
                    match('\n'); 
                    _channel=HIDDEN;

                    }
                    break;
                case 4 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:45:9: '\\r'
                    {
                    match('\r'); 
                    _channel=HIDDEN;

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:8: ( T__6 | T__7 | T__8 | T__9 | INT | COMMENT )
        int alt6=6;
        switch ( input.LA(1) ) {
        case ':':
            {
            alt6=1;
            }
            break;
        case '<':
            {
            alt6=2;
            }
            break;
        case '-':
            {
            alt6=3;
            }
            break;
        case '>':
            {
            alt6=4;
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt6=5;
            }
            break;
        case '\n':
        case '\r':
        case '/':
            {
            alt6=6;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 6, 0, input);

            throw nvae;
        }

        switch (alt6) {
            case 1 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:10: T__6
                {
                mT__6(); 

                }
                break;
            case 2 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:15: T__7
                {
                mT__7(); 

                }
                break;
            case 3 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:20: T__8
                {
                mT__8(); 

                }
                break;
            case 4 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:25: T__9
                {
                mT__9(); 

                }
                break;
            case 5 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:30: INT
                {
                mINT(); 

                }
                break;
            case 6 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:1:34: COMMENT
                {
                mCOMMENT(); 

                }
                break;

        }

    }


 

}