package simulationControl.parsers;

import java.util.Vector;

import network.Node;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;

import antlr.RecognitionException;


public class SimulationFileReader {
	private simulationParserParser parser;
	
    public SimulationFileReader(String path) throws Exception {
    	 simulationParserLexer lex = new simulationParserLexer(new ANTLRFileStream(path));
         CommonTokenStream tokens = new CommonTokenStream(lex);

         parser = new simulationParserParser(tokens);
         parser.initial();  
       
    }
    
    public int getRequests() {
		return parser.getRequests();
	}
	public int getRsaType() {
		return parser.getRsaType();
	}
	public String getRoutingAlgo() {
		return parser.getRoutingAlgo();
	}
	public String getSpectrumAssignAlgo() {
		return parser.getSpectrumAssignAlgo();
	}
	public String getRsaIntegratedAlgo() {
		return parser.getRsaIntegratedAlgo();
	}
	public int getLoadPoints() {
		return parser.getLoadPoints();
	}
	public int getReplications() {
		return parser.getReplications();
	}
	public double getSignificance() {
		return parser.getSignificance();
	}
    
    
}