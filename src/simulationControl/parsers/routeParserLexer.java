// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g 2014-11-11 14:44:19

package simulationControl.parsers;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class routeParserLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int ID=4;
    public static final int COMMENT=5;

    // delegates
    // delegators

    public routeParserLexer() {;} 
    public routeParserLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public routeParserLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g"; }

    // $ANTLR start "T__6"
    public final void mT__6() throws RecognitionException {
        try {
            int _type = T__6;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:7:6: ( '\\n' )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:7:8: '\\n'
            {
            match('\n'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__6"

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:8:6: ( '\\r' )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:8:8: '\\r'
            {
            match('\r'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:9:6: ( '\\r\\n' )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:9:8: '\\r\\n'
            {
            match("\r\n"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:10:6: ( '\\t' )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:10:8: '\\t'
            {
            match('\t'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:44:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:44:7: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:44:40: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='/') ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1=='/') ) {
                    alt5=1;
                }
                else if ( (LA5_1=='*') ) {
                    alt5=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
                    {
                    match("//"); 

                    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:14: (~ ( '\\n' | '\\r' ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>='\u0000' && LA2_0<='\t')||(LA2_0>='\u000B' && LA2_0<='\f')||(LA2_0>='\u000E' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:14: ~ ( '\\n' | '\\r' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:28: ( '\\r' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0=='\r') ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:48:28: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 
                    _channel=HIDDEN;

                    }
                    break;
                case 2 :
                    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:49:9: '/*' ( options {greedy=false; } : . )* '*/'
                    {
                    match("/*"); 

                    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:49:14: ( options {greedy=false; } : . )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0=='*') ) {
                            int LA4_1 = input.LA(2);

                            if ( (LA4_1=='/') ) {
                                alt4=2;
                            }
                            else if ( ((LA4_1>='\u0000' && LA4_1<='.')||(LA4_1>='0' && LA4_1<='\uFFFF')) ) {
                                alt4=1;
                            }


                        }
                        else if ( ((LA4_0>='\u0000' && LA4_0<=')')||(LA4_0>='+' && LA4_0<='\uFFFF')) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:49:42: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    match("*/"); 

                    _channel=HIDDEN;

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:8: ( T__6 | T__7 | T__8 | T__9 | ID | COMMENT )
        int alt6=6;
        switch ( input.LA(1) ) {
        case '\n':
            {
            alt6=1;
            }
            break;
        case '\r':
            {
            int LA6_2 = input.LA(2);

            if ( (LA6_2=='\n') ) {
                alt6=3;
            }
            else {
                alt6=2;}
            }
            break;
        case '\t':
            {
            alt6=4;
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt6=5;
            }
            break;
        case '/':
            {
            alt6=6;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 6, 0, input);

            throw nvae;
        }

        switch (alt6) {
            case 1 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:10: T__6
                {
                mT__6(); 

                }
                break;
            case 2 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:15: T__7
                {
                mT__7(); 

                }
                break;
            case 3 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:20: T__8
                {
                mT__8(); 

                }
                break;
            case 4 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:25: T__9
                {
                mT__9(); 

                }
                break;
            case 5 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:30: ID
                {
                mID(); 

                }
                break;
            case 6 :
                // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:1:33: COMMENT
                {
                mCOMMENT(); 

                }
                break;

        }

    }


 

}