package simulationControl.parsers;

import java.util.Vector;

import network.Node;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;

import antlr.RecognitionException;


public class NetworkFileReader {

    public static Vector<Node> readNetwork(String path) throws Exception {
        networkParserLexer lex = new networkParserLexer(new ANTLRFileStream(path));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        networkParserParser g = new networkParserParser(tokens);
      
        g.initial();
        
        return g.getNodes();    
        
        
       
    }
}