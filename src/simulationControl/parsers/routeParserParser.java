// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g 2014-11-11 14:44:19

package simulationControl.parsers;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class routeParserParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ID", "COMMENT", "'\\n'", "'\\r'", "'\\r\\n'", "'\\t'"
    };
    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int ID=4;
    public static final int COMMENT=5;

    // delegates
    // delegators


        public routeParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public routeParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return routeParserParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g"; }


    private List<List<String>> routes = new ArrayList<>();

    public List<List<String>> getRoutes(){
    	return routes;
    }



    // $ANTLR start "initial"
    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:22:1: initial : ( routeDescription ( '\\n' | '\\r' | '\\r\\n' ) )* ;
    public final void initial() throws RecognitionException {
        try {
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:22:9: ( ( routeDescription ( '\\n' | '\\r' | '\\r\\n' ) )* )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:23:2: ( routeDescription ( '\\n' | '\\r' | '\\r\\n' ) )*
            {
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:23:2: ( routeDescription ( '\\n' | '\\r' | '\\r\\n' ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:23:3: routeDescription ( '\\n' | '\\r' | '\\r\\n' )
            	    {
            	    pushFollow(FOLLOW_routeDescription_in_initial34);
            	    routeDescription();

            	    state._fsp--;

            	    if ( (input.LA(1)>=6 && input.LA(1)<=8) ) {
            	        input.consume();
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "initial"


    // $ANTLR start "routeDescription"
    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:27:1: routeDescription : p= ID '\\t' (m= ID '\\t' )* u= ID ;
    public final void routeDescription() throws RecognitionException {
        Token p=null;
        Token m=null;
        Token u=null;


        	List<String> route = new ArrayList<>();
        	
        try {
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:31:2: (p= ID '\\t' (m= ID '\\t' )* u= ID )
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:33:2: p= ID '\\t' (m= ID '\\t' )* u= ID
            {
            p=(Token)match(input,ID,FOLLOW_ID_in_routeDescription67); 
            route.add((p!=null?p.getText():null));
            match(input,9,FOLLOW_9_in_routeDescription70); 
            // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:33:35: (m= ID '\\t' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==ID) ) {
                    int LA2_1 = input.LA(2);

                    if ( (LA2_1==9) ) {
                        alt2=1;
                    }


                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\ialle_000\\workspace\\tonetsSlice\\gramaticasANTLR\\routeParser.g:33:36: m= ID '\\t'
            	    {
            	    m=(Token)match(input,ID,FOLLOW_ID_in_routeDescription77); 
            	    match(input,9,FOLLOW_9_in_routeDescription79); 
            	    route.add((m!=null?m.getText():null));

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            u=(Token)match(input,ID,FOLLOW_ID_in_routeDescription88); 
            route.add((u!=null?u.getText():null));
            	
            	routes.add(route);
            	
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "routeDescription"

    // Delegated rules


 

    public static final BitSet FOLLOW_routeDescription_in_initial34 = new BitSet(new long[]{0x00000000000001C0L});
    public static final BitSet FOLLOW_set_in_initial36 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ID_in_routeDescription67 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_routeDescription70 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ID_in_routeDescription77 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_routeDescription79 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ID_in_routeDescription88 = new BitSet(new long[]{0x0000000000000002L});

}