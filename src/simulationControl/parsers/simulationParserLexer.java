// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g 2014-07-07 16:27:54

package simulationControl.parsers;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class simulationParserLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int INTEGRATED=4;
    public static final int SEQUENCIAL=5;
    public static final int REQUESTSTAG=6;
    public static final int REPLICATIONSTAG=7;
    public static final int RSATAG=8;
    public static final int LOADINCTAG=9;
    public static final int SIGNIFICANCETAG=10;
    public static final int INT=11;
    public static final int ID=12;
    public static final int FLOAT=13;
    public static final int EXPONENT=14;
    public static final int COMMENT=15;
    public static final int WS=16;

    // delegates
    // delegators

    public simulationParserLexer() {;} 
    public simulationParserLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public simulationParserLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g"; }

    // $ANTLR start "INTEGRATED"
    public final void mINTEGRATED() throws RecognitionException {
        try {
            int _type = INTEGRATED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:7:12: ( 'integrated' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:7:14: 'integrated'
            {
            match("integrated"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INTEGRATED"

    // $ANTLR start "SEQUENCIAL"
    public final void mSEQUENCIAL() throws RecognitionException {
        try {
            int _type = SEQUENCIAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:8:12: ( 'sequencial' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:8:14: 'sequencial'
            {
            match("sequencial"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SEQUENCIAL"

    // $ANTLR start "REQUESTSTAG"
    public final void mREQUESTSTAG() throws RecognitionException {
        try {
            int _type = REQUESTSTAG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:9:13: ( 'requests' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:9:15: 'requests'
            {
            match("requests"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REQUESTSTAG"

    // $ANTLR start "REPLICATIONSTAG"
    public final void mREPLICATIONSTAG() throws RecognitionException {
        try {
            int _type = REPLICATIONSTAG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:10:17: ( 'replications' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:10:19: 'replications'
            {
            match("replications"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REPLICATIONSTAG"

    // $ANTLR start "RSATAG"
    public final void mRSATAG() throws RecognitionException {
        try {
            int _type = RSATAG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:11:8: ( 'rsa' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:11:10: 'rsa'
            {
            match("rsa"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RSATAG"

    // $ANTLR start "LOADINCTAG"
    public final void mLOADINCTAG() throws RecognitionException {
        try {
            int _type = LOADINCTAG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:12:12: ( 'loadPoints' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:12:14: 'loadPoints'
            {
            match("loadPoints"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LOADINCTAG"

    // $ANTLR start "SIGNIFICANCETAG"
    public final void mSIGNIFICANCETAG() throws RecognitionException {
        try {
            int _type = SIGNIFICANCETAG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:13:17: ( 'significance' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:13:19: 'significance'
            {
            match("significance"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SIGNIFICANCETAG"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:14:7: ( '\\n' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:14:9: '\\n'
            {
            match('\n'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:15:7: ( ':' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:15:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:16:7: ( ';' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:16:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:168:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:168:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:168:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:171:5: ( ( '0' .. '9' )+ )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:171:7: ( '0' .. '9' )+
            {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:171:7: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:171:7: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT )
            int alt9=3;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )?
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:9: ( '0' .. '9' )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:10: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);

                    match('.'); 
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:25: ( '0' .. '9' )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( ((LA4_0>='0' && LA4_0<='9')) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:26: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:37: ( EXPONENT )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0=='E'||LA5_0=='e') ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:175:37: EXPONENT
                            {
                            mEXPONENT(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:176:9: '.' ( '0' .. '9' )+ ( EXPONENT )?
                    {
                    match('.'); 
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:176:13: ( '0' .. '9' )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:176:14: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:176:25: ( EXPONENT )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='E'||LA7_0=='e') ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:176:25: EXPONENT
                            {
                            mEXPONENT(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:177:9: ( '0' .. '9' )+ EXPONENT
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:177:9: ( '0' .. '9' )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:177:10: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);

                    mEXPONENT(); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "FLOAT"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='/') ) {
                int LA13_1 = input.LA(2);

                if ( (LA13_1=='/') ) {
                    alt13=1;
                }
                else if ( (LA13_1=='*') ) {
                    alt13=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
                    {
                    match("//"); 

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:14: (~ ( '\\n' | '\\r' ) )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( ((LA10_0>='\u0000' && LA10_0<='\t')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\uFFFF')) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:14: ~ ( '\\n' | '\\r' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:28: ( '\\r' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='\r') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:181:28: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 
                    _channel=HIDDEN;

                    }
                    break;
                case 2 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:182:9: '/*' ( options {greedy=false; } : . )* '*/'
                    {
                    match("/*"); 

                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:182:14: ( options {greedy=false; } : . )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0=='*') ) {
                            int LA12_1 = input.LA(2);

                            if ( (LA12_1=='/') ) {
                                alt12=2;
                            }
                            else if ( ((LA12_1>='\u0000' && LA12_1<='.')||(LA12_1>='0' && LA12_1<='\uFFFF')) ) {
                                alt12=1;
                            }


                        }
                        else if ( ((LA12_0>='\u0000' && LA12_0<=')')||(LA12_0>='+' && LA12_0<='\uFFFF')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:182:42: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    match("*/"); 

                    _channel=HIDDEN;

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "EXPONENT"
    public final void mEXPONENT() throws RecognitionException {
        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:186:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:186:12: ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:186:22: ( '+' | '-' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='+'||LA14_0=='-') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:186:33: ( '0' .. '9' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='0' && LA15_0<='9')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:186:34: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "EXPONENT"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:189:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:189:9: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
            if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:8: ( INTEGRATED | SEQUENCIAL | REQUESTSTAG | REPLICATIONSTAG | RSATAG | LOADINCTAG | SIGNIFICANCETAG | T__17 | T__18 | T__19 | ID | INT | FLOAT | COMMENT | WS )
        int alt16=15;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:10: INTEGRATED
                {
                mINTEGRATED(); 

                }
                break;
            case 2 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:21: SEQUENCIAL
                {
                mSEQUENCIAL(); 

                }
                break;
            case 3 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:32: REQUESTSTAG
                {
                mREQUESTSTAG(); 

                }
                break;
            case 4 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:44: REPLICATIONSTAG
                {
                mREPLICATIONSTAG(); 

                }
                break;
            case 5 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:60: RSATAG
                {
                mRSATAG(); 

                }
                break;
            case 6 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:67: LOADINCTAG
                {
                mLOADINCTAG(); 

                }
                break;
            case 7 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:78: SIGNIFICANCETAG
                {
                mSIGNIFICANCETAG(); 

                }
                break;
            case 8 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:94: T__17
                {
                mT__17(); 

                }
                break;
            case 9 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:100: T__18
                {
                mT__18(); 

                }
                break;
            case 10 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:106: T__19
                {
                mT__19(); 

                }
                break;
            case 11 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:112: ID
                {
                mID(); 

                }
                break;
            case 12 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:115: INT
                {
                mINT(); 

                }
                break;
            case 13 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:119: FLOAT
                {
                mFLOAT(); 

                }
                break;
            case 14 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:125: COMMENT
                {
                mCOMMENT(); 

                }
                break;
            case 15 :
                // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:1:133: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA9 dfa9 = new DFA9(this);
    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA9_eotS =
        "\5\uffff";
    static final String DFA9_eofS =
        "\5\uffff";
    static final String DFA9_minS =
        "\2\56\3\uffff";
    static final String DFA9_maxS =
        "\1\71\1\145\3\uffff";
    static final String DFA9_acceptS =
        "\2\uffff\1\2\1\1\1\3";
    static final String DFA9_specialS =
        "\5\uffff}>";
    static final String[] DFA9_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\3\1\uffff\12\1\13\uffff\1\4\37\uffff\1\4",
            "",
            "",
            ""
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }
        public String getDescription() {
            return "174:1: FLOAT : ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT );";
        }
    }
    static final String DFA16_eotS =
        "\1\uffff\4\10\4\uffff\1\24\3\uffff\6\10\2\uffff\5\10\1\41\6\10"+
        "\1\uffff\26\10\1\76\5\10\1\uffff\2\10\1\106\1\107\2\10\1\112\2\uffff"+
        "\2\10\1\uffff\1\115\1\116\2\uffff";
    static final String DFA16_eofS =
        "\117\uffff";
    static final String DFA16_minS =
        "\1\11\1\156\2\145\1\157\4\uffff\1\56\3\uffff\1\164\1\161\1\147"+
        "\1\160\2\141\2\uffff\1\145\1\165\1\156\1\165\1\154\1\60\1\144\1"+
        "\147\1\145\1\151\1\145\1\151\1\uffff\1\120\1\162\1\156\1\146\1\163"+
        "\1\143\1\157\1\141\1\143\1\151\1\164\1\141\1\151\1\164\1\151\1\143"+
        "\1\163\1\164\1\156\1\145\2\141\1\60\1\151\1\164\1\144\1\154\1\156"+
        "\1\uffff\1\157\1\163\2\60\1\143\1\156\1\60\2\uffff\1\145\1\163\1"+
        "\uffff\2\60\2\uffff";
    static final String DFA16_maxS =
        "\1\172\1\156\1\151\1\163\1\157\4\uffff\1\145\3\uffff\1\164\1\161"+
        "\1\147\1\161\2\141\2\uffff\1\145\1\165\1\156\1\165\1\154\1\172\1"+
        "\144\1\147\1\145\1\151\1\145\1\151\1\uffff\1\120\1\162\1\156\1\146"+
        "\1\163\1\143\1\157\1\141\1\143\1\151\1\164\1\141\1\151\1\164\1\151"+
        "\1\143\1\163\1\164\1\156\1\145\2\141\1\172\1\151\1\164\1\144\1\154"+
        "\1\156\1\uffff\1\157\1\163\2\172\1\143\1\156\1\172\2\uffff\1\145"+
        "\1\163\1\uffff\2\172\2\uffff";
    static final String DFA16_acceptS =
        "\5\uffff\1\10\1\11\1\12\1\13\1\uffff\1\15\1\16\1\17\6\uffff\1\10"+
        "\1\14\14\uffff\1\5\34\uffff\1\3\7\uffff\1\1\1\2\2\uffff\1\6\2\uffff"+
        "\1\7\1\4";
    static final String DFA16_specialS =
        "\117\uffff}>";
    static final String[] DFA16_transitionS = {
            "\1\14\1\5\2\uffff\1\14\22\uffff\1\14\15\uffff\1\12\1\13\12"+
            "\11\1\6\1\7\5\uffff\32\10\4\uffff\1\10\1\uffff\10\10\1\1\2\10"+
            "\1\4\5\10\1\3\1\2\7\10",
            "\1\15",
            "\1\16\3\uffff\1\17",
            "\1\20\15\uffff\1\21",
            "\1\22",
            "",
            "",
            "",
            "",
            "\1\12\1\uffff\12\11\13\uffff\1\12\37\uffff\1\12",
            "",
            "",
            "",
            "\1\25",
            "\1\26",
            "\1\27",
            "\1\31\1\30",
            "\1\32",
            "\1\33",
            "",
            "",
            "\1\34",
            "\1\35",
            "\1\36",
            "\1\37",
            "\1\40",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "\1\42",
            "\1\43",
            "\1\44",
            "\1\45",
            "\1\46",
            "\1\47",
            "",
            "\1\50",
            "\1\51",
            "\1\52",
            "\1\53",
            "\1\54",
            "\1\55",
            "\1\56",
            "\1\57",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\63",
            "\1\64",
            "\1\65",
            "\1\66",
            "\1\67",
            "\1\70",
            "\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "\1\77",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "",
            "\1\104",
            "\1\105",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "\1\110",
            "\1\111",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "",
            "",
            "\1\113",
            "\1\114",
            "",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
            "",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( INTEGRATED | SEQUENCIAL | REQUESTSTAG | REPLICATIONSTAG | RSATAG | LOADINCTAG | SIGNIFICANCETAG | T__17 | T__18 | T__19 | ID | INT | FLOAT | COMMENT | WS );";
        }
    }
 

}