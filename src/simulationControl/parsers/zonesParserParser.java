// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g 2014-11-26 13:27:02

package simulationControl.parsers;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class zonesParserParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "INT", "COMMENT", "':'", "'<'", "'-'", "'>'"
    };
    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int INT=4;
    public static final int COMMENT=5;

    // delegates
    // delegators


        public zonesParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public zonesParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return zonesParserParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g"; }


    private List<int[]> zones = new ArrayList<int[]>();

    public List<int[]> getZones(){
    	return zones;
    }



    // $ANTLR start "initial"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:22:1: initial : ( zone )+ ;
    public final void initial() throws RecognitionException {
        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:22:9: ( ( zone )+ )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:23:3: ( zone )+
            {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:23:3: ( zone )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==INT) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:23:3: zone
            	    {
            	    pushFollow(FOLLOW_zone_in_initial35);
            	    zone();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "initial"


    // $ANTLR start "zone"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:26:1: zone : slot= INT ':' '<' ini= INT '-' fim= INT '>' ;
    public final void zone() throws RecognitionException {
        Token slot=null;
        Token ini=null;
        Token fim=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:26:6: (slot= INT ':' '<' ini= INT '-' fim= INT '>' )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\zonesParser.g:27:2: slot= INT ':' '<' ini= INT '-' fim= INT '>'
            {
            slot=(Token)match(input,INT,FOLLOW_INT_in_zone53); 
            match(input,6,FOLLOW_6_in_zone55); 
            match(input,7,FOLLOW_7_in_zone57); 
            ini=(Token)match(input,INT,FOLLOW_INT_in_zone63); 
            match(input,8,FOLLOW_8_in_zone65); 
            fim=(Token)match(input,INT,FOLLOW_INT_in_zone71); 
            match(input,9,FOLLOW_9_in_zone73); 

            		int aux[] = new int[3];
            		aux[0] = Integer.parseInt((slot!=null?slot.getText():null));
            		aux[1] = Integer.parseInt((ini!=null?ini.getText():null));
            		aux[2] = Integer.parseInt((fim!=null?fim.getText():null));
            		zones.add(aux);
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "zone"

    // Delegated rules


 

    public static final BitSet FOLLOW_zone_in_initial35 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_INT_in_zone53 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_6_in_zone55 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_7_in_zone57 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INT_in_zone63 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_8_in_zone65 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_INT_in_zone71 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_zone73 = new BitSet(new long[]{0x0000000000000002L});

}