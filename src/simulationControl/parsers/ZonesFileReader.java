package simulationControl.parsers;

import java.util.List;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;


public class ZonesFileReader {

    public static List<int[]> readTrafic(String path) throws Exception {
        zonesParserLexer lex = new zonesParserLexer(new ANTLRFileStream(path));
        CommonTokenStream tokens = new CommonTokenStream(lex);
        
        
        zonesParserParser g = new zonesParserParser(tokens);
        
        g.initial();
        
        return g.getZones();    
       
    }
}