// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g 2014-07-13 15:23:16

package simulationControl.parsers;
import java.util.HashMap;
import java.util.Vector;
import network.Pair;
import network.Node;
import network.RequestGenerator;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class traficParserParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ID", "FLOAT", "EXPONENT", "COMMENT", "'\\n'", "'->'", "'\\t'", "';'", "'Gbps'", "'Mbps'"
    };
    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int ID=4;
    public static final int FLOAT=5;
    public static final int EXPONENT=6;
    public static final int COMMENT=7;

    // delegates
    // delegators


        public traficParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public traficParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return traficParserParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g"; }


    private Vector<Pair> pairs;
    private HashMap<String,Node> nodes;
    private Pair actualPair;

    public Vector<Pair> getPairs() {
    	return pairs;
    }
    public void setNodes(Vector<Node> nodes){
    	this.nodes = new HashMap<String,Node>();
    	for(Node n : nodes){
    		this.nodes.put(n.getName(),n);
    	}
    }




    // $ANTLR start "initial"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:35:1: initial : ( descricaoGeradoresPar )* ;
    public final void initial() throws RecognitionException {

        	pairs = new Vector<Pair>();

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:39:2: ( ( descricaoGeradoresPar )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:40:3: ( descricaoGeradoresPar )*
            {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:40:3: ( descricaoGeradoresPar )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:40:4: descricaoGeradoresPar
            	    {
            	    pushFollow(FOLLOW_descricaoGeradoresPar_in_initial43);
            	    descricaoGeradoresPar();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "initial"


    // $ANTLR start "descricaoGeradoresPar"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:44:1: descricaoGeradoresPar : par '\\n' ( descricaoGerador '\\n' )* ;
    public final void descricaoGeradoresPar() throws RecognitionException {
        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:45:2: ( par '\\n' ( descricaoGerador '\\n' )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:46:3: par '\\n' ( descricaoGerador '\\n' )*
            {
            pushFollow(FOLLOW_par_in_descricaoGeradoresPar60);
            par();

            state._fsp--;

            match(input,8,FOLLOW_8_in_descricaoGeradoresPar62); 
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:46:12: ( descricaoGerador '\\n' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==10) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:46:14: descricaoGerador '\\n'
            	    {
            	    pushFollow(FOLLOW_descricaoGerador_in_descricaoGeradoresPar66);
            	    descricaoGerador();

            	    state._fsp--;

            	    match(input,8,FOLLOW_8_in_descricaoGeradoresPar68); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "descricaoGeradoresPar"


    // $ANTLR start "par"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:50:1: par : src= ID '->' dest= ID ;
    public final void par() throws RecognitionException {
        Token src=null;
        Token dest=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:50:5: (src= ID '->' dest= ID )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:51:3: src= ID '->' dest= ID
            {
            src=(Token)match(input,ID,FOLLOW_ID_in_par88); 
            match(input,9,FOLLOW_9_in_par90); 
            dest=(Token)match(input,ID,FOLLOW_ID_in_par96); 

            			Node o = nodes.get((src!=null?src.getText():null));
            			Node d = nodes.get((dest!=null?dest.getText():null));
            			actualPair = new Pair(o,d);	
            			pairs.add(actualPair);	
            		

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "par"


    // $ANTLR start "descricaoGerador"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:60:1: descricaoGerador : '\\t' lb= larguraDeBanda ';' arrive= FLOAT ';' hold= FLOAT ';' inc= FLOAT ;
    public final void descricaoGerador() throws RecognitionException {
        Token arrive=null;
        Token hold=null;
        Token inc=null;
        Double lb = null;


        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:61:2: ( '\\t' lb= larguraDeBanda ';' arrive= FLOAT ';' hold= FLOAT ';' inc= FLOAT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:62:3: '\\t' lb= larguraDeBanda ';' arrive= FLOAT ';' hold= FLOAT ';' inc= FLOAT
            {
            match(input,10,FOLLOW_10_in_descricaoGerador114); 
            pushFollow(FOLLOW_larguraDeBanda_in_descricaoGerador120);
            lb=larguraDeBanda();

            state._fsp--;

            match(input,11,FOLLOW_11_in_descricaoGerador122); 
            arrive=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_descricaoGerador128); 
            match(input,11,FOLLOW_11_in_descricaoGerador130); 
            hold=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_descricaoGerador136); 
            match(input,11,FOLLOW_11_in_descricaoGerador138); 
            inc=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_descricaoGerador144); 

            			RequestGenerator rg = new RequestGenerator(actualPair, lb, Double.parseDouble((hold!=null?hold.getText():null)),Double.parseDouble((arrive!=null?arrive.getText():null)), Double.parseDouble((inc!=null?inc.getText():null)));
            			actualPair.addRequestGenerator(rg);
            		

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "descricaoGerador"


    // $ANTLR start "larguraDeBanda"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:69:1: larguraDeBanda returns [Double bandwidth] : f= FLOAT ( ( 'Gbps' ) | ( 'Mbps' ) ) ;
    public final Double larguraDeBanda() throws RecognitionException {
        Double bandwidth = null;

        Token f=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:70:2: (f= FLOAT ( ( 'Gbps' ) | ( 'Mbps' ) ) )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:3: f= FLOAT ( ( 'Gbps' ) | ( 'Mbps' ) )
            {
            f=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_larguraDeBanda171); 
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:13: ( ( 'Gbps' ) | ( 'Mbps' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            else if ( (LA3_0==13) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:14: ( 'Gbps' )
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:14: ( 'Gbps' )
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:15: 'Gbps'
                    {
                    match(input,12,FOLLOW_12_in_larguraDeBanda175); 
                    bandwidth = Double.parseDouble((f!=null?f.getText():null)) * 1073741824.0;

                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:82: ( 'Mbps' )
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:82: ( 'Mbps' )
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\traficParser.g:71:83: 'Mbps'
                    {
                    match(input,13,FOLLOW_13_in_larguraDeBanda181); 
                    bandwidth = Double.parseDouble((f!=null?f.getText():null)) * 1048576.0;

                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return bandwidth;
    }
    // $ANTLR end "larguraDeBanda"

    // Delegated rules


 

    public static final BitSet FOLLOW_descricaoGeradoresPar_in_initial43 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_par_in_descricaoGeradoresPar60 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_8_in_descricaoGeradoresPar62 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_descricaoGerador_in_descricaoGeradoresPar66 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_8_in_descricaoGeradoresPar68 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_ID_in_par88 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_par90 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ID_in_par96 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_10_in_descricaoGerador114 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_larguraDeBanda_in_descricaoGerador120 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_descricaoGerador122 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_FLOAT_in_descricaoGerador128 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_descricaoGerador130 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_FLOAT_in_descricaoGerador136 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_descricaoGerador138 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_FLOAT_in_descricaoGerador144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOAT_in_larguraDeBanda171 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_larguraDeBanda175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_larguraDeBanda181 = new BitSet(new long[]{0x0000000000000002L});

}