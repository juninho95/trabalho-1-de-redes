// $ANTLR 3.3 Nov 30, 2010 12:45:30 C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g 2014-07-07 16:27:54

package simulationControl.parsers;
import simulator.Simulation;
import rsa.RSA;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class simulationParserParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "INTEGRATED", "SEQUENCIAL", "REQUESTSTAG", "REPLICATIONSTAG", "RSATAG", "LOADINCTAG", "SIGNIFICANCETAG", "INT", "ID", "FLOAT", "EXPONENT", "COMMENT", "WS", "'\\n'", "':'", "';'"
    };
    public static final int EOF=-1;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int INTEGRATED=4;
    public static final int SEQUENCIAL=5;
    public static final int REQUESTSTAG=6;
    public static final int REPLICATIONSTAG=7;
    public static final int RSATAG=8;
    public static final int LOADINCTAG=9;
    public static final int SIGNIFICANCETAG=10;
    public static final int INT=11;
    public static final int ID=12;
    public static final int FLOAT=13;
    public static final int EXPONENT=14;
    public static final int COMMENT=15;
    public static final int WS=16;

    // delegates
    // delegators


        public simulationParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public simulationParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return simulationParserParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g"; }


    	
    	private int requests;
    	private int rsaType;
    	private String routingAlgo;
    	private String spectrumAssignAlgo;
    	private String rsaIntegratedAlgo;
    	private int loadPoints;
    	private int replications;
    	private double significance;
    	
    	private boolean breq = false, brsa = false, bload = false, brep = false, bsig = false;
    	
    	
    	public int getRequests() {
    		return requests;
    	}
    	public int getRsaType() {
    		return rsaType;
    	}
    	public String getRoutingAlgo() {
    		return routingAlgo;
    	}
    	public String getSpectrumAssignAlgo() {
    		return spectrumAssignAlgo;
    	}
    	public String getRsaIntegratedAlgo() {
    		return rsaIntegratedAlgo;
    	}
    	public int getLoadPoints() {
    		return loadPoints;
    	}
    	public int getReplications() {
    		return replications;
    	}
    	public double getSignificance() {
    		return significance;
    	}
    	
    	



    // $ANTLR start "initial"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:70:1: initial : ( ( request | rsaDefinition | load | replications | significance ) '\\n' )* ;
    public final void initial() throws RecognitionException {
        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:70:8: ( ( ( request | rsaDefinition | load | replications | significance ) '\\n' )* )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:2: ( ( request | rsaDefinition | load | replications | significance ) '\\n' )*
            {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:2: ( ( request | rsaDefinition | load | replications | significance ) '\\n' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=REQUESTSTAG && LA2_0<=SIGNIFICANCETAG)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:3: ( request | rsaDefinition | load | replications | significance ) '\\n'
            	    {
            	    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:3: ( request | rsaDefinition | load | replications | significance )
            	    int alt1=5;
            	    switch ( input.LA(1) ) {
            	    case REQUESTSTAG:
            	        {
            	        alt1=1;
            	        }
            	        break;
            	    case RSATAG:
            	        {
            	        alt1=2;
            	        }
            	        break;
            	    case LOADINCTAG:
            	        {
            	        alt1=3;
            	        }
            	        break;
            	    case REPLICATIONSTAG:
            	        {
            	        alt1=4;
            	        }
            	        break;
            	    case SIGNIFICANCETAG:
            	        {
            	        alt1=5;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 1, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt1) {
            	        case 1 :
            	            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:4: request
            	            {
            	            pushFollow(FOLLOW_request_in_initial103);
            	            request();

            	            state._fsp--;


            	            }
            	            break;
            	        case 2 :
            	            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:14: rsaDefinition
            	            {
            	            pushFollow(FOLLOW_rsaDefinition_in_initial107);
            	            rsaDefinition();

            	            state._fsp--;


            	            }
            	            break;
            	        case 3 :
            	            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:30: load
            	            {
            	            pushFollow(FOLLOW_load_in_initial111);
            	            load();

            	            state._fsp--;


            	            }
            	            break;
            	        case 4 :
            	            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:37: replications
            	            {
            	            pushFollow(FOLLOW_replications_in_initial115);
            	            replications();

            	            state._fsp--;


            	            }
            	            break;
            	        case 5 :
            	            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:72:52: significance
            	            {
            	            pushFollow(FOLLOW_significance_in_initial119);
            	            significance();

            	            state._fsp--;


            	            }
            	            break;

            	    }

            	    match(input,17,FOLLOW_17_in_initial122); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            		if(breq && brsa && bload && brep && bsig){
            		}else{
            			throw new RecognitionException();
            		}	
            		/*System.out.println("rsaType: " + rsaType);
            		System.out.println("rout: " + routingAlgo);		
            		System.out.println("spec: " + spectrumAssignAlgo);*/
            		
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "initial"


    // $ANTLR start "request"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:87:1: request : REQUESTSTAG ':' quant= INT ;
    public final void request() throws RecognitionException {
        Token quant=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:87:9: ( REQUESTSTAG ':' quant= INT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:88:2: REQUESTSTAG ':' quant= INT
            {

            		if(breq) throw new RecognitionException();
            		else breq = true;
            	
            match(input,REQUESTSTAG,FOLLOW_REQUESTSTAG_in_request148); 
            match(input,18,FOLLOW_18_in_request150); 
            quant=(Token)match(input,INT,FOLLOW_INT_in_request156); 
             
            				     requests = Integer.parseInt((quant!=null?quant.getText():null));
            				   

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "request"


    // $ANTLR start "rsaDefinition"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:97:1: rsaDefinition : RSATAG ':' ( ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID ) | ( INTEGRATED ';' integrated= ID ) ) ;
    public final void rsaDefinition() throws RecognitionException {
        Token routing=null;
        Token spectrumAssign=null;
        Token integrated=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:98:2: ( RSATAG ':' ( ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID ) | ( INTEGRATED ';' integrated= ID ) ) )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:99:2: RSATAG ':' ( ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID ) | ( INTEGRATED ';' integrated= ID ) )
            {

            		
            		if(brsa){ throw new RecognitionException();}
            		else{ 
            			brsa = true;
            			
            		}
            	
            match(input,RSATAG,FOLLOW_RSATAG_in_rsaDefinition174); 
            match(input,18,FOLLOW_18_in_rsaDefinition176); 
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:107:13: ( ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID ) | ( INTEGRATED ';' integrated= ID ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==SEQUENCIAL) ) {
                alt3=1;
            }
            else if ( (LA3_0==INTEGRATED) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:107:14: ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID )
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:107:14: ( SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID )
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:107:15: SEQUENCIAL ';' routing= ID ';' spectrumAssign= ID
                    {
                    match(input,SEQUENCIAL,FOLLOW_SEQUENCIAL_in_rsaDefinition180); 
                    match(input,19,FOLLOW_19_in_rsaDefinition182); 
                    routing=(Token)match(input,ID,FOLLOW_ID_in_rsaDefinition188); 
                    match(input,19,FOLLOW_19_in_rsaDefinition190); 
                    spectrumAssign=(Token)match(input,ID,FOLLOW_ID_in_rsaDefinition196); 

                    }


                    										
                    										rsaType = RSA.RSA_SEQUENCIAL;
                    										
                    										try{
                    											
                    											routingAlgo = (routing!=null?routing.getText():null);
                    											
                    											spectrumAssignAlgo = (spectrumAssign!=null?spectrumAssign.getText():null);
                    										}catch(Exception ex){
                    											throw new RecognitionException();
                    										
                    										}
                    									 

                    }
                    break;
                case 2 :
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:122:7: ( INTEGRATED ';' integrated= ID )
                    {
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:122:7: ( INTEGRATED ';' integrated= ID )
                    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:122:8: INTEGRATED ';' integrated= ID
                    {
                    match(input,INTEGRATED,FOLLOW_INTEGRATED_in_rsaDefinition209); 
                    match(input,19,FOLLOW_19_in_rsaDefinition211); 
                    integrated=(Token)match(input,ID,FOLLOW_ID_in_rsaDefinition217); 

                    }


                    		   						
                    		   						rsaType = RSA.RSA_INTEGRATED;
                    		   						try{
                    									
                    									rsaIntegratedAlgo = (integrated!=null?integrated.getText():null);
                    								}catch(Exception ex){
                    									throw new RecognitionException();										
                    								}		   
                    		   					
                    		   					

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "rsaDefinition"


    // $ANTLR start "load"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:136:1: load : LOADINCTAG ':' linc= INT ;
    public final void load() throws RecognitionException {
        Token linc=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:136:6: ( LOADINCTAG ':' linc= INT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:137:2: LOADINCTAG ':' linc= INT
            {

            		if(bload) throw new RecognitionException();
            		else bload = true;
            	
            match(input,LOADINCTAG,FOLLOW_LOADINCTAG_in_load240); 
            match(input,18,FOLLOW_18_in_load242); 
            linc=(Token)match(input,INT,FOLLOW_INT_in_load248); 

            					loadPoints = Integer.parseInt((linc!=null?linc.getText():null));					
            				 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "load"


    // $ANTLR start "replications"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:146:1: replications : REPLICATIONSTAG ':' r= INT ;
    public final void replications() throws RecognitionException {
        Token r=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:147:2: ( REPLICATIONSTAG ':' r= INT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:148:2: REPLICATIONSTAG ':' r= INT
            {

            		if(brep) throw new RecognitionException();
            		else brep = true;
            	
            match(input,REPLICATIONSTAG,FOLLOW_REPLICATIONSTAG_in_replications266); 
            match(input,18,FOLLOW_18_in_replications268); 
            r=(Token)match(input,INT,FOLLOW_INT_in_replications274); 

            					replications = Integer.parseInt((r!=null?r.getText():null));
            				   

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "replications"


    // $ANTLR start "significance"
    // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:157:1: significance : SIGNIFICANCETAG ':' d= FLOAT ;
    public final void significance() throws RecognitionException {
        Token d=null;

        try {
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:158:2: ( SIGNIFICANCETAG ':' d= FLOAT )
            // C:\\Users\\IallenGábio\\workspace\\tonetsSlice\\gramaticasANTLR\\simulationParser.g:159:2: SIGNIFICANCETAG ':' d= FLOAT
            {

            		if(bsig) throw new RecognitionException();
            		else bsig = true;
            	
            match(input,SIGNIFICANCETAG,FOLLOW_SIGNIFICANCETAG_in_significance292); 
            match(input,18,FOLLOW_18_in_significance294); 
            d=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_significance300); 

            				     	significance = Double.parseDouble((d!=null?d.getText():null));
            				     

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "significance"

    // Delegated rules


 

    public static final BitSet FOLLOW_request_in_initial103 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rsaDefinition_in_initial107 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_load_in_initial111 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_replications_in_initial115 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_significance_in_initial119 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_initial122 = new BitSet(new long[]{0x00000000000007C2L});
    public static final BitSet FOLLOW_REQUESTSTAG_in_request148 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_request150 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_INT_in_request156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RSATAG_in_rsaDefinition174 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rsaDefinition176 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_SEQUENCIAL_in_rsaDefinition180 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rsaDefinition182 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_ID_in_rsaDefinition188 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rsaDefinition190 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_ID_in_rsaDefinition196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGRATED_in_rsaDefinition209 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_rsaDefinition211 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_ID_in_rsaDefinition217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOADINCTAG_in_load240 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_load242 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_INT_in_load248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REPLICATIONSTAG_in_replications266 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_replications268 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_INT_in_replications274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SIGNIFICANCETAG_in_significance292 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_significance294 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_FLOAT_in_significance300 = new BitSet(new long[]{0x0000000000000002L});

}