package simulationControl.parsers;

import java.util.List;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;


public class FixedRoutesFileReader {

    public static List<List<String>> readRoutes(String path) throws Exception {
        routeParserLexer lex = new routeParserLexer(new ANTLRFileStream(path));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        routeParserParser r = new routeParserParser(tokens);
        
        
        r.initial();
        
        return r.getRoutes();    
       
    }
}