package simulationControl.parsers;

import java.util.Vector;

import network.Node;
import network.Pair;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;


public class TraficFileReader {

    public static Vector<Pair> readTrafic(String path, Vector<Node> nodes) throws Exception {
        traficParserLexer lex = new traficParserLexer(new ANTLRFileStream(path));
        CommonTokenStream tokens = new CommonTokenStream(lex);

        traficParserParser g = new traficParserParser(tokens);
        g.setNodes(nodes);
        
        g.initial();
        
        return g.getPairs();    
       
    }
}