package rsa;

import java.util.Vector;

import network.ControlPlane;
import request.Request;
import rsa.integrated.CompleteSharing;
import rsa.integrated.DedicatedPartition;
import rsa.integrated.IntegratedRSAAlgoritm;
import rsa.integrated.LoadBalancedDedicatedPartition;
import rsa.integrated.PseudoPartition;
import rsa.integrated.ZonePartition;
import rsa.integrated.ZonePartitionTopInvasion;
import rsa.modulation.Modulation;
import rsa.modulation.ModulationSelector;
import rsa.routing.DJK;
import rsa.routing.FixedRoutes;
import rsa.routing.FuzzyExampleRouting;
import rsa.routing.RoutingInterface;
import rsa.spectrumAssignment.BestFit;
import rsa.spectrumAssignment.ExactFit;
import rsa.spectrumAssignment.FirstFit;
import rsa.spectrumAssignment.SpectrumAssignmentAlgoritm;

/**
 * Esta classe devera ser responsavel por executar os algoritmos de RSA, verificando se o algoritmo selecionado eh do tipo
 * integrado ou sequencial, apos acionar o/os algoritmo/s necessarios para alocar o recurso para a requisicao
 * @author Iallen
 *
 */
public class RSA {
	//Constantes que indicam que tipo s�o os algoritmos RSA (sequenciais ou integrados)
	public static final int RSA_SEQUENCIAL = 0;
	public static final int RSA_INTEGRATED = 1;

	//Constantes para indica��o dos algoritmos RSA
	//Roteamento 
	public static final String ROUTING_DJK = "djk";
	public static final String ROUTING_FIXEDROUTES = "fixedroutes";
	public static final String ROUTING_FUZZY = "fuzzyExample";
	
	//Aloca��o de espectro 
	public static final String SPECTRUM_ASSIGNMENT_FISTFIT = "firstfit";
	public static final String SPECTRUM_ASSIGNMENT_BESTFIT = "bestfit";
	public static final String SPECTRUM_ASSIGNMENT_WORSTFIT = "worstfit";
	public static final String SPECTRUM_ASSIGNMENT_EXACTFIT = "exactfit";
	public static final String SPECTRUM_ASSIGNMENT_ZONEFIRSTBESTFIT = "zonefirstbestfit";
	public static final String SPECTRUM_ASSIGNMENT_ZONEFIRSTFIRSTFIT = "zonefirstfirstfit";
	public static final String SPECTRUM_ASSIGNMENT_DEDICATEDZONEFIRSTFIT = "dedicatedzonefirstfit";
	
	//Integrados 
	public static final String INTEGRATED_COMPLETESHARING = "completesharing";
	public static final String INTEGRATED_PSEUDOPARTITION = "pseudopartition";
	public static final String INTEGRATED_DEDICATEDPARTITION = "dedicatedpartition";
	public static final String INTEGRATED_LOADBALANCEDDEDICATEDPARTITION = "loadbalanceddedicatedpartition";
	public static final String INTEGRATED_ZONEPARTITION = "zonepartition";
	public static final String INTEGRATED_ZONEPARTITIONTOPINVASION = "zonepartitiontopinvasion";
	
	//fim das constantes
	
	private int rsaType;
	private RoutingInterface routing;
	private SpectrumAssignmentAlgoritm spectrumAssignment;
	private IntegratedRSAAlgoritm integrated;
	private ModulationSelector modulationSelector;

	public RSA(String integrated, double spectrumBand) throws Exception{
		this.rsaType = 1;
		instantiateIntegratedRSA(integrated);
		modulationSelector = new ModulationSelector(spectrumBand);
	}
	
	public RSA(String routingType, String spectrumAssignmentType, double spectrumBand) throws Exception{
		this.rsaType = 0;
		instantiateRouting(routingType);
		instantiateSpectrumAssignment(spectrumAssignmentType);	
		modulationSelector = new ModulationSelector(spectrumBand);
	}
	
	/**
	 * instancia o algoritmo de roteamento
	 * @param routingType
	 * @throws Exception
	 */
	private void instantiateRouting(String routingType) throws Exception{
		switch(routingType){
			case ROUTING_DJK:
				this.routing = new DJK();
			break;
			case ROUTING_FUZZY:
				this.routing = new FuzzyExampleRouting();
			break;
			case ROUTING_FIXEDROUTES:
				this.routing = new FixedRoutes();
			break;
			
			default:
				throw new Exception("unknow routing algorithm");
		}
	}
	
	/**
	 * instancia o algoritmo de aloca��o de espectro
	 * @param spectrumAssignmentType
	 * @throws Exception
	 */
	private void instantiateSpectrumAssignment(String spectrumAssignmentType) throws Exception{
		
		switch(spectrumAssignmentType){
			case SPECTRUM_ASSIGNMENT_FISTFIT:
				this.spectrumAssignment = new FirstFit();
			break;
			case SPECTRUM_ASSIGNMENT_BESTFIT:
				this.spectrumAssignment = new BestFit();
			break;
			case SPECTRUM_ASSIGNMENT_EXACTFIT:
				this.spectrumAssignment = new ExactFit();
			break;
			
			default:
				throw new Exception("unknow spectrum assignment algorithm");
		}
	}
	
	/**
	 * instancia o algoritmo de RSA integrado
	 * @param integratedRSAType
	 * @throws Exception
	 */
	private void instantiateIntegratedRSA(String integratedRSAType) throws Exception{
		switch(integratedRSAType){			
			case INTEGRATED_COMPLETESHARING:
				this.integrated = new CompleteSharing();
			break;
			case INTEGRATED_PSEUDOPARTITION:
				this.integrated = new PseudoPartition();
			break;
			case INTEGRATED_DEDICATEDPARTITION:
				this.integrated = new DedicatedPartition();
			break;
			case INTEGRATED_LOADBALANCEDDEDICATEDPARTITION:
				this.integrated = new LoadBalancedDedicatedPartition();
				break;
			case INTEGRATED_ZONEPARTITION:
				this.integrated = new ZonePartition();
			break;
			case INTEGRATED_ZONEPARTITIONTOPINVASION:
				this.integrated = new ZonePartitionTopInvasion();
			break;
		
			default:
				throw new Exception("unknow integrated RSA algorithm");
		}
	}
	
	/**
	 * Este m�todo tenta atender uma determinada requisi��o alocando os devidos recursos para a mesma
	 * @param request
	 * @return
	 */
	public boolean atenderRequisicao(Request request){
		
		switch(rsaType){
			case RSA_INTEGRATED:
				return integrated.rsa(request);
			case RSA_SEQUENCIAL:
				if(routing.findRoute(request, ControlPlane.getControlPlane().getMesh())){
					Modulation mod = modulationSelector.selectModulation(request);
					request.setModulation(mod);
					return spectrumAssignment.assignSpectrum(mod.requiredSlots(request.getRequiredBandwidth()), request);
				}else{
					return false;
				}						
		}
		
		return false;
	}
	
	/**
	 * Retorna as rotas para cada par(o,d) na rede
	 * m�todo utilizado apenas para roteamento fixo
	 * @return
	 */
	public Vector<Route> getRoutesForAllPairs() {
		DJK djk = (DJK) this.routing;
		return djk.getRoutesForAllPairs();
		
	}

}
