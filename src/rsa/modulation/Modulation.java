package rsa.modulation;

public class Modulation {

	private String name;
	
	private double bitsPerSimbol;
	/*
	 * max range in Km
	 */
	private double maxRange;
	
	private double freqSlot;
	
	public Modulation(String name, double bitsPerSimbol, double freq, double maxRange){
		this.name = name;
		this.bitsPerSimbol = bitsPerSimbol;
		this.maxRange = maxRange;
		this.freqSlot = freq;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the bitsPerSimbol
	 */
	public double getBitsPerSimbol() {
		return bitsPerSimbol;
	}

	/**
	 * @return the maxRange
	 */
	public double getMaxRange() {
		return maxRange;
	}
	
	/**
	 * retorna a quantidade de slots necess�rios de acordo com a largura de banda
	 * @param bandwidth
	 * @return
	 */
	public int requiredSlots(double bandwidth){		
		//System.out.println("C = " + bandwidth + "    bm = " + this.bitsPerSimbol + "     fslot = " + this.freqSlot);
		
		double res = bandwidth/(this.bitsPerSimbol * this.freqSlot);
		
		//System.out.println("res = " + res);
		
		int res2 = (int) res;
		
		if(res-res2!=0.0){
			res2++;
		}
		
		
		
		return res2;
	}
	
	
	
}
