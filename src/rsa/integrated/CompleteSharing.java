package rsa.integrated;

import java.util.List;

import network.ControlPlane;
import network.Link;
import request.Request;
import rsa.KMenores;
import rsa.NewKMenores;
import rsa.Route;
import rsa.modulation.Modulation;
import rsa.modulation.ModulationSelector;
import rsa.spectrumAssignment.FirstFit;
import util.IntersectionFreeSpectrum;


public class CompleteSharing implements IntegratedRSAAlgoritm{

	private NewKMenores kMenores;
	private ModulationSelector modulationSelector;
	
	@Override
	public boolean rsa(Request request) {
		if(kMenores==null) kMenores = new NewKMenores(ControlPlane.getControlPlane().getMesh(), 3); //este algoritmo utiliza 3 caminhos alternativos
		if(modulationSelector==null) modulationSelector = new ModulationSelector(ControlPlane.getControlPlane().getMesh().getLinkList().get(0).getSlotSpectrumBand());
		
		
		List<Route> candidateRoutes = kMenores.getRoutes(request.getSource(), request.getDestination());
		Route rotaEscolhida = null;
		int faixaEscolhida[] = {999999,999999}; //valor jamais atingido
		
		for (Route r : candidateRoutes) {
			//calcular quantos slots s�o necess�rios para esta rota
			request.setRoute(r);
			Modulation mod = modulationSelector.selectModulation(request);
			
			List<int[]> merge = IntersectionFreeSpectrum.merge(r);
			
			int ff[] = FirstFit.firstFit(mod.requiredSlots(request.getRequiredBandwidth()), merge);
			
			if(ff!=null && ff[0]<faixaEscolhida[0]){
				faixaEscolhida = ff;
				rotaEscolhida = r;				
			}
		}
		
		if(rotaEscolhida!=null){ //se n�o houver rota escolhida � por que n�o foi encontrado recurso dispon�vel em nenhuma das rotas candidatas
			request.setRoute(rotaEscolhida);
			request.setModulation(modulationSelector.selectModulation(request));
			request.setSpectrumAssigned(faixaEscolhida);
			
		
			return true;
			
		}else{
			request.setRoute(candidateRoutes.get(0));
			request.setModulation(modulationSelector.selectModulation(request));
			return false;
		}
		
	}

}
