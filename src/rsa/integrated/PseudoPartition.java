package rsa.integrated;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import network.ControlPlane;
import network.Link;
import request.Request;
import rsa.KMenores;
import rsa.NewKMenores;
import rsa.Route;
import rsa.modulation.Modulation;
import rsa.modulation.ModulationSelector;
import rsa.spectrumAssignment.FirstFit;
import rsa.spectrumAssignment.LastFit;
import util.IntersectionFreeSpectrum;


public class PseudoPartition implements IntegratedRSAAlgoritm{

	/**
	 * Larguras de banda que utilizam o espectro de cima para baixo
	 */
	private HashSet<Double> largBandSuperiores;
	
	private NewKMenores kMenores;
	private ModulationSelector modulationSelector;
	
	public PseudoPartition(){
		largBandSuperiores = new HashSet<>();
		largBandSuperiores.add(343597383680.0); //320Gbps
	}
	
	@Override
	public boolean rsa(Request request) {
		if(kMenores==null) kMenores = new NewKMenores(ControlPlane.getControlPlane().getMesh(), 3); //este algoritmo utiliza 3 caminhos alternativos
		if(modulationSelector==null) modulationSelector = new ModulationSelector(ControlPlane.getControlPlane().getMesh().getLinkList().get(0).getSlotSpectrumBand());
		
		List<Route> candidateRoutes = kMenores.getRoutes(request.getSource(), request.getDestination());
		Route rotaEscolhida = null;
		int faixaEscolhida[] =  new int[2];
		//verificar se o firstfit deve ser aplicado de baixo para cima ou de cima para baixo
		if(!largBandSuperiores.contains(request.getRequiredBandwidth())){ // alocar de baixo para cima
			faixaEscolhida[0] = 9999999;
			faixaEscolhida[1] = 9999999;
			
			for (Route r : candidateRoutes) {
				//calcular quantos slots s�o necess�rios para esta rota
				request.setRoute(r);
				Modulation mod = modulationSelector.selectModulation(request);
				
				List<int[]> merge = IntersectionFreeSpectrum.merge(r);
				
				int ff[] = FirstFit.firstFit(mod.requiredSlots(request.getRequiredBandwidth()), merge);
				
				if(ff!=null && ff[0]<faixaEscolhida[0]){
					faixaEscolhida = ff;
					rotaEscolhida = r;				
				}
			}
			
		}else{ //alocar de cima para baixo
			
			
			faixaEscolhida[0] = -1;
			faixaEscolhida[1] = -1;
			
			for (Route r : candidateRoutes) {
				//calcular quantos slots s�o necess�rios para esta rota
				request.setRoute(r);
				Modulation mod = modulationSelector.selectModulation(request);
				
				List<int[]> merge = IntersectionFreeSpectrum.merge(r);
				
				int lf[] = LastFit.lastFit(mod.requiredSlots(request.getRequiredBandwidth()), merge);
				
				if(lf!=null && lf[1]>faixaEscolhida[1]){
					faixaEscolhida = lf;
					rotaEscolhida = r;	
					
					
				}
			}
			
		}
		
		if(rotaEscolhida!=null){ //se n�o houver rota escolhida � por que n�o foi encontrado recurso dispon�vel em nenhuma das rotas candidatas
			request.setRoute(rotaEscolhida);
			request.setModulation(modulationSelector.selectModulation(request));
			request.setSpectrumAssigned(faixaEscolhida);
			
			
			return true;
			
		}else{
			request.setRoute(candidateRoutes.get(0));
			request.setModulation(modulationSelector.selectModulation(request));
			return false;
		}
		
	}

}
