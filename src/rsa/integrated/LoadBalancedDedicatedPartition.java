package rsa.integrated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import network.ControlPlane;
import network.Link;
import request.Request;
import rsa.KMenores;
import rsa.NewKMenores;
import rsa.Route;
import rsa.modulation.Modulation;
import rsa.modulation.ModulationSelector;
import rsa.spectrumAssignment.FirstFit;
import simulationControl.Util;
import simulationControl.parsers.ZonesFileReader;
import util.IntersectionFreeSpectrum;


public class LoadBalancedDedicatedPartition implements IntegratedRSAAlgoritm{

	private NewKMenores kMenores;
	private ModulationSelector modulationSelector;
	
	private HashMap<Integer, int[]> zones; 
	
	public LoadBalancedDedicatedPartition(){
		List<int[]> zones=null;
		try {
			zones = ZonesFileReader.readTrafic(Util.projectPath + "/zones");
		} catch (Exception e) {
			System.out.println("n�o foi poss�vel ler o arquivo com a especifica��o das zonas!");
			
			//e.printStackTrace();
		}
		this.zones = new HashMap<>();
		int aux[];
		for (int[] zone : zones) {
			aux = new int[2];
			aux[0] = zone[1];
			aux[1] = zone[2];
			this.zones.put(zone[0], aux);			
		}
		
	}
	
	@Override
	public boolean rsa(Request request) {
		if(kMenores==null) kMenores = new NewKMenores(ControlPlane.getControlPlane().getMesh(), 3); //este algoritmo utiliza 3 caminhos alternativos
		if(modulationSelector==null) modulationSelector = new ModulationSelector(ControlPlane.getControlPlane().getMesh().getLinkList().get(0).getSlotSpectrumBand());
		
		
		List<Route> candidateRoutes = kMenores.getRoutes(request.getSource(), request.getDestination());
		Route rotaEscolhida = null;
		int faixaEscolhida[] = {999999,999999}; //valor jamais atingido
		int menosUsado = 999999999;
		
		for (Route r : candidateRoutes) {
			//calcular quantos slots s�o necess�rios para esta rota
			request.setRoute(r);
			Modulation mod = modulationSelector.selectModulation(request);
			
			
			
			int quantSlots = mod.requiredSlots(request.getRequiredBandwidth());
			int zone[] = this.zones.get(quantSlots);
			List<int[]> primaryZone = new ArrayList<>();
			primaryZone.add(zone);			
			
			List<int[]> merge = IntersectionFreeSpectrum.merge(r);
			merge = IntersectionFreeSpectrum.merge(merge, primaryZone);
			
			
			
			int ff[] = FirstFit.firstFit(quantSlots, merge);
			
			int ut = this.quantSlotsUsadosZona(r, zone);
			
			if(ff!=null && ut<menosUsado){
				faixaEscolhida = ff;
				rotaEscolhida = r;	
				menosUsado = ut;
			}
		}
		
		if(rotaEscolhida!=null){ //se n�o houver rota escolhida � por que n�o foi encontrado recurso dispon�vel em nenhuma das rotas candidatas
			request.setRoute(rotaEscolhida);
			request.setModulation(modulationSelector.selectModulation(request));
			request.setSpectrumAssigned(faixaEscolhida);
			
			return true;
			
		}else{
			request.setRoute(candidateRoutes.get(0));
			request.setModulation(modulationSelector.selectModulation(request));
			return false;
		}
		
	}
	
	/**
	 * retorna o somat�rio do quadrado da quantidade de slots utilizados em cada link de uma rota em uma determinada zona
	 * @param r
	 * @param zone
	 * @return
	 */
	private int quantSlotsUsadosZona(Route r, int zone[]){
		int res = 0;
		List<int[]> zoneAux = new ArrayList<int[]>();
		zoneAux.add(zone);
		
		for (Link link : r.getLinkList()) {
			List<int[]> merge = IntersectionFreeSpectrum.merge(link.getFreeSpectrumBands(), zoneAux);
			
			int livres = 0;
			for (int[] is : merge) {
				livres += (is[1] - is[0] + 1);
			}
			int usados = link.getNumOfSlots() - livres;
			
			res += (usados*usados);
			
		}
		
		
		return res;
	}

}
