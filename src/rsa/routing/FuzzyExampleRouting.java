package rsa.routing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import network.ControlPlane;
import network.Mesh;
import network.Node;
import request.Request;
import rsa.Route;
import rsa.modulation.Modulation;
import rsa.modulation.ModulationSelector;
import util.CalculadorFragmentacao;
import util.IntersectionFreeSpectrum;
import util.ShortestPaths;



public class FuzzyExampleRouting implements RoutingInterface {

	private static final String DIV = "-";
	private HashMap<String, ArrayList<Route>> allShortestPaths = new HashMap<>();
	static String filename = "simulations/Fuzzy/tipper.fcl";
	public static int contRotasDisponiveisNaoAlocadas = 0;
	public static boolean intervaloDisponivel = false;
	
	public FuzzyExampleRouting() {

	}
	
	@Override
	public boolean findRoute(Request request, Mesh mesh) {
		Route r;
		ArrayList<Route> rotasDoPar = new ArrayList<>();
		
		if(allShortestPaths.isEmpty()){ //Busca menores caminhos
			allShortestPaths = ShortestPaths.calculaMenoresRotas(mesh);
		}
		
		Node source = request.getSource();
		Node destination = request.getDestination();
		
		rotasDoPar = allShortestPaths.get(source.getName() + DIV + destination.getName()); //Descomentar para menores caminhos
		
		r = defuzzificacaoMenorCusto(rotasDoPar, request);
		
		if(r!=null){
			request.setRoute(r);
			return true;
		}
		return false;
	}
	
	private int DistDeSlotsOcupadosAdjacentes(List<int[]> slotsContig,int qntdSlots){
		
		int menor = 400;
		
		int tam,desloc;
		for (int[] is : slotsContig) {
			tam = is[1] - is[0] + 1;	//Qntd de slots dispon�veis
			if(tam >= qntdSlots){	//Verifica se pode atender � requisi��o
				desloc = tam - qntdSlots;
				if(menor > desloc){
					menor = desloc;
				}
			}
		}		
		return menor;
	}
	/*Encontra o intervalo dispon�vel mais pr�ximo da borda espectral*/
	private int indexMaisProximoDaBorda(List<int[]> comp, int quantSlots){
		int menor = 400, maior = 0, indexMenor, indexMaior, tam;
		for(int[] a : comp)	{
			tam = a[1] - a[0] + 1;	//Qntd de slots dispon�veis
			if(tam >= quantSlots){	//Verifica se pode atender � requisi��o
				
					if(a[0]  <=200 ){
						if(a[0] < menor){
							menor = a[0];
						}
					}else{
						if(a[1] > maior){
							maior = a[1];
						}
					}
			}
		}
		indexMenor = menor/200;
		indexMaior = 1 - ((maior-200)/(400-200));
		return (indexMenor < indexMaior)?indexMenor:indexMaior;
	}
	
	private Route defuzzificacaoMenorCusto(ArrayList<Route> rotasDoPar, Request request) {
		Route rotaEscolhida = null;
		double val = 0.0, fragRel;
		double menorCusto = 10.0;
		List<int[]> composition;
		ModulationSelector modulationSelector = new ModulationSelector(ControlPlane.getControlPlane().getMesh().getLinkList().get(0).getSlotSpectrumBand());
		Modulation mod;
		int quantSlots;
		
		FIS fis = FIS.load(filename, true);
		
		if (fis == null) {
			System.err.println("Can't load file: '" + filename + "'");
			System.exit(1);
		}

		// Defini��es da Biblioteca Fuzzy - n�o mudar!
		FunctionBlock fb = fis.getFunctionBlock(null);
		
		intervaloDisponivel = false;
		for (Route rotaAtual : rotasDoPar) {
		
			//Criar enlace de intersec��o para calcular fragmenta��o
			composition = IntersectionFreeSpectrum.merge(rotaAtual);
			
			//setar rota atual como escolhida, para c�lculo da quantidade de slots necess�rios
			request.setRoute(rotaAtual);
			mod = modulationSelector.selectModulation(request);
			quantSlots = mod.requiredSlots(request.getRequiredBandwidth());
			
			if(!possuiIntervaloDisponivel(composition, quantSlots)){
				continue;
			}
			
			//calcular fragmenta��o relativa para a rota atual
			fragRel = CalculadorFragmentacao.fragmentacaoRelativa(composition, quantSlots);			
			fragRel = 1.0 - fragRel; //Complemento (Inverso) da Fragmenta��o Relativa
			
			 
			
			// Setar entrada para sistema Fuzzy HEREEE adicinar a fun��o de calculo da parada lah e atribuir � variavel qtslot
			fb.setVariable("fragment", fragRel);
			fb.setVariable("qtslot", this.DistDeSlotsOcupadosAdjacentes(composition, quantSlots));
			fb.setVariable("indexSlot", this.indexMaisProximoDaBorda(composition, quantSlots));
//			fb.setVariable("indexSlot", indexPrimeiroSlotLivreNaRota(rotaAtual, quantSlots));
	
			
			// Avalia��o Fuzzy
			fb.evaluate();
	
			// Retorno Fuzzy
			fb.getVariable("tip").defuzzify();
			
			val = fb.getVariable("tip").getValue();
			
			//verifica se pelo menos uma rota possui slots para atender a requisi��o
			
			if(val < menorCusto){
				rotaEscolhida = rotaAtual;
				menorCusto = val;
			}
			request.setRoute(null);
		}
		if(rotaEscolhida == null)
			rotaEscolhida = rotasDoPar.get(0);
		return rotaEscolhida;
	}

	/*
	 * Verifica se a rota apresenta intervalo dispon�vel para atender a requisi��o
	 */
	private boolean possuiIntervaloDisponivel(List<int[]> composition, int quantSlots) {
		for(int[] interv : composition){
			if((interv[1] - interv[0] + 1) >= quantSlots)
				return true;
		}
		return false;
	}

	private int quantidadeSlotsOcupadosNaRota(Route rotaAtual) {
		int totalLivre = 0, intervaloAtual = 0;
		List<int[]> composicao;
		int totalSlots = rotaAtual.getLink(0).getNumOfSlots();
				
		composicao = IntersectionFreeSpectrum.merge(rotaAtual);
		
		for(int[] intervalo : composicao){
			intervaloAtual = intervalo[1] - intervalo[0] + 1;
			totalLivre += intervaloAtual;
		}
		
		return totalSlots - totalLivre;
	}
	
	private int indexPrimeiroSlotLivreNaRota(Route rotaAtual, int quantSlots) {
		List<int[]> composicao;
		int totalSlots = rotaAtual.getLink(0).getNumOfSlots();
				
		composicao = IntersectionFreeSpectrum.merge(rotaAtual);
		
		for(int[] intervalo : composicao){
			if((intervalo[1] - intervalo[0]) >= quantSlots)
				return intervalo[0];
		}
		
		return totalSlots;
	}
}