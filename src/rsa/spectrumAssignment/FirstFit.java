package rsa.spectrumAssignment;

import java.util.ArrayList;
import java.util.List;

import network.Link;
import request.Request;
import rsa.Route;
import util.IntersectionFreeSpectrum;

/**
 * Esta � uma classe que faz atribui��o de espectro seguindo a pol�tica first fit
 * @author Iallen
 *
 */
public class FirstFit implements SpectrumAssignmentAlgoritm{

	public FirstFit(){
		
	}
	
	@Override
	public boolean assignSpectrum(int numberOfSlots, Request request) {
		Route route = request.getRoute();		
		List<Link> links = new ArrayList<>(route.getLinkList());		
		List<int[]> composition;
		composition = links.get(0).getFreeSpectrumBands();
		int i;
		IntersectionFreeSpectrum ifs = new IntersectionFreeSpectrum();
		for(i=1;i<links.size();i++){
			composition = ifs.merge(composition, links.get(i).getFreeSpectrumBands());
		}
		
		//agora basta buscar a primeira faixa livre com a quantidade de slots requsiitadas na composi��o gerada
		int chosen[] = firstFit(numberOfSlots, composition);
		
		if(chosen == null) return false; //n�o encontrou nenhuma faixa cont�gua e cont�nua dispon�vel
		
		request.setSpectrumAssigned(chosen);
		
		return true;
	}

	/**
	 * aplica a pol�tica firstFit a uma determinada lista de faixas livres retorna a faixa escolhida
	 * @param numberOfSlots
	 * @param livres
	 * @return
	 */
	public static int[] firstFit(int numberOfSlots, List<int[]> livres){
		int chosen[] = null;
		for (int[] band : livres) {
			if(band[1] - band[0] + 1 >= numberOfSlots){
				chosen = band;
				chosen[1] = chosen[0] + numberOfSlots - 1;//n�o � necess�rio alocar a faixa inteira, apenas a quantidade de slots necess�ria				
				break;
			}
		}
		
		return chosen;
	}
	


}

