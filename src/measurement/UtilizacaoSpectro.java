package measurement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import request.Request;
import network.ControlPlane;
import network.Link;



public class UtilizacaoSpectro {
	public final static String SEP = "-";
	
	private int loadPoint;
	private int replication;
	
	/**
	 * armazena a quantidade de requisi��es geradas em fun��o da quantidade de slots que estas requisi��es exigem
	 */
	private HashMap<Integer, Integer> quantReqPerSlotReq;	
	private int quantRequisicoes;
	
	private HashMap<String, HashMap<Integer,Integer>> quantReqPerSlotReqPerLink;
	private HashMap<String, Integer> quantRequisicoesPerLink;
	
	private double utilizationGen;
	private int obsUtilizacao;	
	private HashMap<String, Double> utilizationPerLink;
	
	
	private int[] desUtilizationPerSlot;
	
	
	
	public UtilizacaoSpectro(int loadPoint, int replication){
			this.loadPoint = loadPoint;
			this.replication = replication;
			
			quantRequisicoes = 0;
			quantReqPerSlotReq = new HashMap<>();
			quantRequisicoesPerLink = new HashMap<>();
			quantReqPerSlotReqPerLink = new HashMap<>();
			
			utilizationGen = 0.0;
			obsUtilizacao = 0;
			utilizationPerLink = new HashMap<String, Double>();
			
			desUtilizationPerSlot = new int[400]; //cuidado ao utilizar links com quantidades de slots diferentes de 200
			
	}
	
	/**
	 * adiciona uma nova observa��o de utiliza��o
	 * @param sucess
	 * @param request
	 */
	public void addNewObservation(Request request){
		this.newObsReqTamFaixaGeral(request);	
		this.newObsReqTamFaixaPerLink(request);
		this.newObsUtilization(request);
	}
	
	/**
	 * observa��o de utiliza��o do recurso espectral da rede
	 * @param request
	 */
	private void newObsUtilization(Request request) {
		//utiliza��o geral e por link
		Double utGeral = 0.0;
		Double utLink;
		for (Link link : ControlPlane.getControlPlane().getMesh().getLinkList()) {
			utGeral += link.getUtilization();
			
			utLink = this.utilizationPerLink.get(link.getSource().getName()+SEP+link.getDestination().getName());
			if(utLink==null) utLink = 0.0;
			utLink += link.getUtilization();
			this.utilizationPerLink.put(link.getSource().getName()+SEP+link.getDestination().getName(), utLink);
			
			//calcular desutiliza��o por slot
			for (int[] faixa : link.getFreeSpectrumBands()) {
				incrementarDesUtFaixa(faixa);
			}
			
		}
		
		utGeral = utGeral / (double)ControlPlane.getControlPlane().getMesh().getLinkList().size();
		
		this.utilizationGen += utGeral;
		
		this.obsUtilizacao++;
		
		
		
	}

	
	private void incrementarDesUtFaixa(int faixa[]){
		int i;
		for(i=faixa[0]-1;i<faixa[1];i++){
			desUtilizationPerSlot[i]++;
		}
	}
	
	
	/** 
	 * observa��o da m�trica de Requisi��es em fun��o do tamanho da faixa requisitado de forma geral
	 * @param request
	 */
	private void newObsReqTamFaixaGeral(Request request){
		quantRequisicoes++;			
		int qSlots = request.getModulation().requiredSlots(request.getRequiredBandwidth());			
		Integer aux = this.quantReqPerSlotReq.get(qSlots);			
		if(aux==null){
			aux = 0;
		}			
		aux++;			
		this.quantReqPerSlotReq.put(qSlots, aux);		
	}
	
	/** 
	 * observa��o da m�trica de Requisi��es em fun��o do tamanho da faixa requisitado por link
	 * @param request
	 */
	private void newObsReqTamFaixaPerLink(Request request){
		for (Link link : request.getRoute().getLinkList()) {
			newObsReqTamFaixaPerLink(link, request);
		}	
	}
	
	private void newObsReqTamFaixaPerLink(Link link, Request request){
		String l = link.getSource().getName() + SEP + link.getDestination().getName();
		//incrementar a quantidade de requisi��es geradas
		Integer aux = this.quantRequisicoesPerLink.get(l);
		if(aux == null) aux = 0;
		aux++;
		this.quantRequisicoesPerLink.put(l, aux);
		
		//incrementar a quantidade de requisi��es geradas com este tamanho de faixa requisitado
		int qSlots = request.getModulation().requiredSlots(request.getRequiredBandwidth());
		HashMap<Integer, Integer> hashAux = quantReqPerSlotReqPerLink.get(l);
		if(hashAux==null){
			hashAux = new HashMap<Integer, Integer>();
			quantReqPerSlotReqPerLink.put(l, hashAux);
		}
		aux = hashAux.get(qSlots);
		if(aux==null) aux = 0;
		aux++;
		hashAux.put(qSlots, aux);		
	}
	
	
	
	/**
	 * retorna uma lista contendo os valores de tamanhos de faixa que tiveram pelo menos uma requisi��o
	 * @return
	 */
	public List<Integer> getQuantidadesDeSlots(){
		ArrayList<Integer> res = new ArrayList(this.quantReqPerSlotReq.keySet());
		
		return res;
	}
	
	/**
	 * retorna o percentual de requisi��es entre as que foram geradas que exigiram uma faixa livre de tamanho passado por par�metro
	 * @param tamanhoFaixaReq
	 * @return
	 */
	public double getPercentualReq(int tamanhoFaixaReq){				
		return ((double)this.quantReqPerSlotReq.get(tamanhoFaixaReq))/((double)this.quantRequisicoes);
	}
	
	public Set<String> getLinkSet(){
		return this.quantReqPerSlotReqPerLink.keySet();		
	}
	
	public List<Integer> getQuantidadesDeSlotsPorLink(String link){
		ArrayList<Integer> res = new ArrayList(this.quantReqPerSlotReqPerLink.get(link).keySet());
		
		return res;
	}
	
	public double getPercentualReq(String link, int tamanhoFaixaReq){
		double d = this.quantReqPerSlotReqPerLink.get(link).get(tamanhoFaixaReq);
		double n = this.quantRequisicoesPerLink.get(link);
		
		return d/n;
	}

	public double getUtilizationGen(){
		return this.utilizationGen / (double) this.obsUtilizacao;		
	}
	
	public double getUtilizationPerLink(String link){
		return this.utilizationPerLink.get(link) / (double) this.obsUtilizacao;		
	}
	
	public double getUtilizationPerSlot(int Slot){
		double desUt = (double)desUtilizationPerSlot[Slot-1] / ((double)this.obsUtilizacao * ControlPlane.getControlPlane().getMesh().getLinkList().size());
		
		return 1 - desUt;
	}
	
	
	/**
	 * @return the loadPoint
	 */
	public int getLoadPoint() {
		return loadPoint;
	}

	/**
	 * @return the replication
	 */
	public int getReplication() {
		return replication;
	}
	
	
	

}
